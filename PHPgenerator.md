# PHP Code generator

## Dependencies

- Guzzle is used for HTTP(S) communication.
- brick\Math for BigDecimal
- phpunit

## The generated code

Note the generated code assumes autoloading.

Identifiers are derived from the names in the soap definitions by replacing dashes and spaces with underscores.

Code is generated in a namespace with its name derived from the service name.

The generated classes are currently always called.

- SoapClient extends \Wsdl2PhpGenerator\BaseSoapClient
- SoapGenerator extends \Wsdl2PhpGenerator\BaseSoapGenerator
- SoapParser extends \Wsdl2PhpGenerator\BaseSoapParser

There also classes declared for every complex type in the wsdl. All these extend `SoapObject`.

## Exceptions

Currently there are three exception types declared

### SoapException 

extends \Exception

Common base for other exceptions.

### SoapFaultException

extends SoapException

Thrown when the server returns a SoapFault. The message contains the soapfault message. The previous exception will be set to the original `GuzzleHttp\Exception\ServerException`.

### SoapServerException

extends SoapException

Thrown in case of all other server (or proxy errors). The previous exception will be 
set to the original `GuzzleHttp\Exception\ServerException`.



## sidecar file

The sidecar file can be used to adjust classnames, namespaces and file locations.

```
<?xml version="1.0" encoding="UTF-8"?>
<WsdlSidecar>
	<PhpConfig>
		<Client>
			<Path>apiclient</Path>
			<Namespace>ns</Namespace>
			<ClassName>Client</ClassName>
		</Client>
		<Framework>
			<Path>apiclient</Path>
			<Namespace>Wsdl2PhpGenerator</Namespace>
		</Framework>

		<TypeNameMappings>
			<NameMapping>
				<WsdlName>Foo</WsdlName>
				<CodeName>Bar</CodeName>
			</NameMapping>
		</TypeNameMappings>
	</PhpConfig>
</WsdlSidecar>
```
