# wsdl2htmldocs

This is the repository of wsdl2htmldocs a commandline tool for extracting documentation and generating code
from wsdl files and generating an html file describing the operations and types. The tool is written in C#
and targets .NET Core 3.1.

# Building

Make sure you have dotnet core 3.1 installed go [here](https://dotnet.microsoft.com/download) for details. Clone the repo and run `dotnet build` in the toplevel folder.

# Using

The tool has buildin help. Use the `--help` commandline option to view this help. You can use this option with every command and subcommand to get more help on that specific (sub)command. For example `wsdl2htmldocs code php --help` will give you help for generating php code.


### Limitations

Currently the namespaces from the wsdl are not used to generate identifiers in the PHP code (they are used to lookup the correct definitions within the wsdl). This could potentially give collisions of you ever encounter a service that uses multiple namespaces.