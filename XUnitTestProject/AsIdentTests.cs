﻿using wsdl2htmldocs.phpgenerator;

namespace XUnitTestProject;

public class AsIdentTests
{
    [Fact]
    public void AsIdentValidIdentifierUnchanged()
    {
        const string id = "id";
        string result = PhpUtil.AsIdent(id);
        Assert.Equal(id, result);
    }

    [Fact]
    public void AsIdentReplacesDashes()
    {
        const string id = "id-foo-bar";
        string result = PhpUtil.AsIdent(id);
        Assert.Equal("id_foo_bar", result);
    }

    [Fact]
    public void AsIdentReplacesSpaces()
    {
        const string id = "id foo bar";
        string result = PhpUtil.AsIdent(id);
        Assert.Equal("id_foo_bar", result);
    }
}
