﻿using Moq;
using System.Collections.Generic;
using System.CommandLine;
using System.Threading.Tasks;
using wsdl2htmldocs.htmlgenerator;
using wsdl2htmldocs.merge;
using wsdl2htmldocs.phpgenerator;

namespace XUnitTestProject;

public class CommandLineTests
{
    private Mock<IGeneratePhpHandler> generatePhpHandler = new();
    private Mock<IHtmlDocsHandler> generateHtmlDocsHandler = new();
    private Mock<IMergeCommandHandler> mergeHandler = new();

    [Fact]
    public async Task GeneratePhpIsCalled()
    {

        string[] args =
            [
                "1.wsdl",
                "2.wsdl",
                "code",
                "php",
                "--output",
                "foo"
            ];

        GeneratePhpParams? phpParams = null;
        generatePhpHandler.Setup(s => s.Handle(It.IsAny<GeneratePhpParams>()))
            .Callback<GeneratePhpParams>(p => phpParams = p);

        await Invoke(args);
        Assert.NotNull(phpParams);
        Assert.Single(phpParams.Wsdls, w => w.Name == "1.wsdl");
        Assert.Single(phpParams.Wsdls, w => w.Name == "2.wsdl");
        Assert.Equal("foo", phpParams.Output);
        Assert.False(phpParams.AutoDecimalMappings);
        Assert.False(phpParams.RemoveListWrappers);
    }

    [Theory]
    [InlineData(null, true)]
    [InlineData(false, false)]
    [InlineData(true, true)]
    public async Task GeneratePhp_AutoDecimalMappings_NoValue(bool? value, bool expected)
    {

        List<string> args =
            [
                "1.wsdl",
                "code",
                "php",
                "--auto-decimal-mappings"
            ];
        if (value.HasValue)
            args.Add(value.Value.ToString());

        GeneratePhpParams? phpParams = null;
        generatePhpHandler.Setup(s => s.Handle(It.IsAny<GeneratePhpParams>()))
            .Callback<GeneratePhpParams>(p => phpParams = p);

        await Invoke(args.ToArray());
        Assert.NotNull(phpParams);
        Assert.Equal(expected, phpParams.AutoDecimalMappings);
    }

    [Fact]
    public async Task HtmlDocsHandlerIsCalled()
    {

        string[] args =
            [
                "1.wsdl",
                "2.wsdl",
                "docs",
                "html",
                "--output",
                "foo"
            ];

        GenerateHtmlConfig? htmlParams = null;
        generateHtmlDocsHandler.Setup(s => s.Handle(It.IsAny<GenerateHtmlConfig>()))
            .Callback<GenerateHtmlConfig>(p => htmlParams = p);

        await Invoke(args);
        Assert.NotNull(htmlParams);
        Assert.Single(htmlParams.Wsdls, w => w.Name == "1.wsdl");
        Assert.Single(htmlParams.Wsdls, w => w.Name == "2.wsdl");
        Assert.Equal("foo", htmlParams.Output.Name);
    }

    [Fact]
    public async Task MergeHandlerIsCalled()
    {

        string[] args =
            [
                "1.wsdl",
                "2.wsdl",
                "merge",
                "--output",
                "foo"
            ];

        MergeWsdlConfig? config = null;
        mergeHandler.Setup(s => s.Handle(It.IsAny<MergeWsdlConfig>()))
            .Callback<MergeWsdlConfig>(p => config = p);

        await Invoke(args);
        Assert.NotNull(config);
        Assert.Single(config.Wsdls, w => w.Name == "1.wsdl");
        Assert.Single(config.Wsdls, w => w.Name == "2.wsdl");
        Assert.Equal("foo", config.Output.Name);
    }

    private Task<int> Invoke(string[] args)
    {
        return wsdl2htmldocs.Program.CreateRoot(
            generatePhpHandler.Object,
            generateHtmlDocsHandler.Object,
            mergeHandler.Object
            ).InvokeAsync(args);
    }

}
