﻿using System.Xml.Linq;
using wsdl2htmldocs.phpgenerator.config;
using wsdl2htmldocs.soap;

namespace XUnitTestProject;
public class IsListWrapperTests
{
    [Fact]
    public void True()
    {
        ComplexType ct = new(new("MyType"));
        ct.Elements.Add(
            new Element(new("foo"))
            {
                MaxOccurs = -1,
            }
            );

        PhpGeneratorConfiguration config = new(null!, new());
        Assert.True(config.IsListWrapper(ct));
    }

    [Fact]
    public void FalseMultipleElements()
    {
        ComplexType ct = new(new("MyType"));
        ct.Elements.Add(
            new Element(new("foo"))
            {
                MaxOccurs = -1,
            }
            );
        ct.Elements.Add(
            new Element(new("foo"))
            );

        PhpGeneratorConfiguration config = new(null!, new());
        Assert.False(config.IsListWrapper(ct));
    }

    [Fact]
    public void FalseSingleElementNotRepeating()
    {
        ComplexType ct = new(new("MyType"));
        ct.Elements.Add(
            new Element(new("foo"))
            );

        PhpGeneratorConfiguration config = new(null!, new());
        Assert.False(config.IsListWrapper(ct));
    }

    [Fact]
    public void ValidPostfix()
    {
        ComplexType ct = new(new("MyTypeList"));
        ct.Elements.Add(
            new Element(new("foo"))
            );

        PhpGeneratorConfiguration config = new(null!, new());
        config.Sidecar.PhpConfig.ListWrapperDetection = new()
        {
            AllowedTypeNamePostfixes = new() { "List" }
        };
        Assert.False(config.IsListWrapper(ct));
    }

    [Fact]
    public void NoValidPostfix()
    {
        ComplexType ct = new(new("MyTypeRequest"));
        ct.Elements.Add(
            new Element(new("foo"))
            );

        PhpGeneratorConfiguration config = new(null!, new());
        config.Sidecar.PhpConfig.ListWrapperDetection = new()
        {
            AllowedTypeNamePostfixes = new() { "List" }
        };
        Assert.False(config.IsListWrapper(ct));
    }
}
