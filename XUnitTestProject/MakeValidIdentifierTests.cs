﻿using wsdl2htmldocs.phpgenerator;
using Xunit;

namespace XUnitTestProject;

public class MakeValidIdentifierTests
{
    [Fact]
    public void TestValidSymbolReturnedAsIs()
    {
        string input = "valid";
        string output = PhpUtil.MakeValidIdentifier(input);

        Assert.Equal(input, output);
    }

    [Fact]
    public void TestInValidSymbolIsPrefixed()
    {
        string input = "2invalid";
        string output = PhpUtil.MakeValidIdentifier(input);
        string expected = "_2invalid";

        Assert.Equal(expected, output);
    }
}
