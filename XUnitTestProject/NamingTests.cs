using System;
using System.Collections.Generic;
using wsdl2htmldocs.naming;
using Xunit;

namespace XUnitTestProject;

public class NamingTests
{
    [Fact]
    public void TestDetectPascal()
    {
        string input = "PascalStyle";
        var resp = NormalizedName.DetectCaseStyle(input);
        Assert.Equal(CaseStyle.Pascal, resp);
    }
    [Fact]
    public void TestDetectCamelCase()
    {
        string input = "camelCase";
        var resp = NormalizedName.DetectCaseStyle(input);
        Assert.Equal(CaseStyle.Camel, resp);
    }
    [Fact]
    public void TestDetectSnakeCase()
    {
        string input = "snake_case";
        var resp = NormalizedName.DetectCaseStyle(input);
        Assert.Equal(CaseStyle.Snake, resp);
    }
    [Fact]
    public void TestDetectKebabCase()
    {
        string input = "kebab-case";
        var resp = NormalizedName.DetectCaseStyle(input);
        Assert.Equal(CaseStyle.Kebab, resp);
    }
    [Fact]
    public void NormalizePascal()
    {
        string input = "PascalStyle";
        var expected = new List<string>();
        expected.Add("Pascal");
        expected.Add("Style");

        var resp = new NormalizedName(input);
        Assert.Equal(expected, resp.Parts);
        Assert.Equal(input, resp.Original);
    }
    [Fact]
    public void NormalizeSnake()
    {
        string input = "snake_style";
        var expected = new List<string>();
        expected.Add("snake");
        expected.Add("style");

        var resp = new NormalizedName(input);
        Assert.Equal(expected, resp.Parts);
    }
    [Fact]
    public void NormalizeKebab()
    {
        string input = "kebab-style";
        var expected = new List<string>();
        expected.Add("kebab");
        expected.Add("style");

        var resp = new NormalizedName(input);
        Assert.Equal(expected, resp.Parts);
    }
    [Fact]
    public void NormalizeKebabMixedCase()
    {
        string input = "Kebab-Style";
        var expected = new List<string>();
        expected.Add("Kebab");
        expected.Add("Style");

        var resp = new NormalizedName(input);
        Assert.Equal(expected, resp.Parts);
    }
    [Fact]
    public void StartsWithSuccess()
    {
        var inputValue = new NormalizedName("STARTS-WITH-SOMETHING");
        var start = new NormalizedName("starts_with");
        var result = inputValue.StartsWith(start);
        Assert.True(result);
    }
    [Fact]
    public void StartsWithSuccessStringOverload()
    {
        var inputValue = new NormalizedName("STARTS-WITH-SOMETHING");
        var result = inputValue.StartsWith("starts_with");
        Assert.True(result);
    }
    [Fact]
    public void StartsWithSuccessWhenEqual()
    {
        var inputValue = new NormalizedName("STARTS-WITH-SOMETHING");
        var start = new NormalizedName("StartsWithSomething");
        var result = inputValue.StartsWith(start);
        Assert.True(result);
    }
    [Fact]
    public void StartsWithFail()
    {
        var inputValue = new NormalizedName("BEGIN-WITH-SOMETHING");
        var start = new NormalizedName("starts_with");
        var result = inputValue.StartsWith(start);
        Assert.False(result);
    }
    [Fact]
    public void StartsWithFail2()
    {
        var inputValue = new NormalizedName("starts");
        var start = new NormalizedName("starts_with");
        var result = inputValue.StartsWith(start);
        Assert.False(result);
    }
    [Fact]
    public void SubPartsReturnsCorrectParts()
    {
        var inputValue = new NormalizedName("BEGIN-WITH-SOMETHING");
        var result = inputValue.SubParts(1);
        var expected = new NormalizedName("WITH-SOMETHING");
        Assert.Equal(expected.Parts, result.Parts);
    }

    [Fact]
    public void SubPartsLowerBoundCheck()
    {
        var inputValue = new NormalizedName("BEGIN-WITH-SOMETHING");
        inputValue.SubParts(0); // succeeds
        Assert.Throws<ArgumentOutOfRangeException>(
            () => inputValue.SubParts(-1)
            );
    }

    [Fact]
    public void SubPartsUpperBoundCheck()
    {
        var inputValue = new NormalizedName("BEGIN-WITH-SOMETHING");
        inputValue.SubParts(2); // succeeds
        Assert.Throws<ArgumentOutOfRangeException>(
            () => inputValue.SubParts(3)
            );
    }
}
