﻿using System.IO;
using System.Linq;
using System.Text;
using wsdl2htmldocs.parser;
using wsdl2htmldocs.soap;
using Xunit;

namespace XUnitTestProject;


public class ParseTests
{
    const string simpleWsdl =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<definitions name=\"LicentieWebServicePortBinding\"\n" +
        "  targetNamespace=\"http://test.service.nl/\"\n" +
        "  xmlns:tns=\"http://test.service.nl/\"\n" +
        "  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
        "  xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\n" +
        "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
        "  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
        "  xmlns:ns1=\"http://test.service.nl/\"\n" +
        "  xmlns:SOAP=\"http://schemas.xmlsoap.org/wsdl/soap/\"\n" +
        "  xmlns:HTTP=\"http://schemas.xmlsoap.org/wsdl/http/\"\n" +
        "  xmlns:MIME=\"http://schemas.xmlsoap.org/wsdl/mime/\"\n" +
        "  xmlns:DIME=\"http://schemas.xmlsoap.org/ws/2002/04/dime/wsdl/\"\n" +
        "  xmlns:WSDL=\"http://schemas.xmlsoap.org/wsdl/\"\n" +
        "  xmlns=\"http://schemas.xmlsoap.org/wsdl/\">\n" +
        "\n" +
        "<types>\n" +
        "\n" +
        "  <schema targetNamespace=\"http://test.service.nl/\"\n" +
        "    xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
        "    xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\n" +
        "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
        "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
        "    xmlns:ns1=\"http://test.service.nl/\"\n" +
        "    xmlns=\"http://www.w3.org/2001/XMLSchema\"\n" +
        "    elementFormDefault=\"unqualified\"\n" +
        "    attributeFormDefault=\"unqualified\">\n" +
        "\n" +
        "    <import namespace=\"http://schemas.xmlsoap.org/soap/encoding/\"/>\n" +
        "    <complexType name=\"getKey\">\n" +
        "          <sequence>\n" +
        "            <element name=\"licentienr\" type=\"xsd:string\" minOccurs=\"0\" maxOccurs=\"1\"/>\n" +
        "          </sequence>\n" +
        "    </complexType>\n" +
        "    <complexType name=\"getKeyResponse\">\n" +
        "          <sequence>\n" +
        "            <element name=\"return\" type=\"xsd:string\" minOccurs=\"0\" maxOccurs=\"1\"/>\n" +
        "          </sequence>\n" +
        "    </complexType>\n" +
        "\n" +
        "    <element name=\"getKeyResponse\" type=\"ns1:getKeyResponse\"/>\n" +
        "    <element name=\"getKey\" type=\"ns1:getKey\"/>\n" +
        "  </schema>\n" +
        "\n" +
        "</types>\n" +
        "\n" +
        "<message name=\"getKey\">\n" +
        "  <part name=\"Body\" element=\"ns1:getKey\"/>\n" +
        "</message>\n" +
        "\n" +
        "<message name=\"getKeyResponse\">\n" +
        "  <part name=\"Body\" element=\"ns1:getKeyResponse\"/>\n" +
        "</message>\n" +
        "\n" +
        "<portType name=\"LicentieWebService\">\n" +
        "  <operation name=\"getKey\">\n" +
        "    <documentation>Service definition of function __ns1__getKey</documentation>\n" +
        "    <input message=\"tns:getKey\"/>\n" +
        "    <output message=\"tns:getKeyResponse\"/>\n" +
        "  </operation>\n" +
        "</portType>\n" +
        "\n" +
        "<binding name=\"TestPortBinding\" type=\"tns:TestService\">\n" +
        "  <SOAP:binding style=\"document\" transport=\"http://schemas.xmlsoap.org/soap/http\"/>\n" +
        "  <operation name=\"getKey\">\n" +
        "    <SOAP:operation soapAction=\"http://test.service.nl/service\"/>\n" +
        "    <input>\n" +
        "          <SOAP:body use=\"literal\" parts=\"Body\"/>\n" +
        "    </input>\n" +
        "    <output>\n" +
        "          <SOAP:body use=\"literal\" parts=\"Body\"/>\n" +
        "    </output>\n" +
        "  </operation>\n" +
        "</binding>\n" +
        "\n" +
        "<service name=\"Test\">\n" +
        "  <port name=\"TestPortBinding\" binding=\"tns:TestPortBinding\">\n" +
        "    <SOAP:address location=\"http://localhost:80\"/>\n" +
        "  </port>\n" +
        "</service>\n" +
        "\n" +
        "</definitions>\n";

    [Fact]
    public void ParseTest()
    {
        using MemoryStream stream = new();
        stream.Write(Encoding.UTF8.GetBytes(simpleWsdl));
        stream.Seek(0, SeekOrigin.Begin);

        WsdlDocParser parser = new();
        Wsdl wsdl = parser.Parse(stream);

        Assert.NotNull(wsdl);
        Assert.Single(wsdl.PortType.Operations);
        Assert.Single(wsdl.Types.Namespaces());
        Assert.Equal(2, wsdl.Types.TypeNames("http://test.service.nl/").Count());
    }
}
