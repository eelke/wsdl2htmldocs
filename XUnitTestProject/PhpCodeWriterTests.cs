﻿using Castle.Core.Configuration;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wsdl2htmldocs.phpgenerator;
using wsdl2htmldocs.phpgenerator.ir;
using wsdl2htmldocs.phpgenerator.phpsourcegenerator;
using XUnitTestProject.Util;

namespace XUnitTestProject;
public class PhpCodeWriterTests
{
    [Fact]
    public void EnumsEmptyMakeNoClasses()
    {
        List<wsdl2htmldocs.phpgenerator.ir.IrEnum> enums = new();

        var fileBuilder = Substitute.For<IFileBuilder>();

        PhpCodeWriter subject = new(
            null!, //new Configuration(),
            new ClassList(),
            enums,
            null!, //new Wsdl(),
            null! // ClientClass clientClass
            );

        subject.GeneratePhpEnums(fileBuilder);

        fileBuilder.DidNotReceive().Class(Arg.Any<string>(), Arg.Any<Action<IClassBuilder>>());
    }

    [Fact]
    public void GeneratePhpEnumsCallsFileBuilderClass()
    {
        const string enumName = "FooEnum";
        const string constName = "FooBar";
        const string constValue = "FOO_BAR";

        List<wsdl2htmldocs.phpgenerator.ir.IrEnum> enums = new();
        enums.Add(new()
        {
            CodeName = enumName,
            WsdlName = enumName,
            Values = new() {
                new(constName, constValue ),
            }
        });

        var classBuilder = Substitute.For<IClassBuilder>();

        var builderFactory = Substitute.For<IBuilderFactory>();
        builderFactory.ClassBuilder(Arg.Any<IFileBuilder>())
            .Returns(classBuilder);

        FileBuilder fileBuilder = new(
            Substitute.For<TextWriter>(),
            null,
            builderFactory
            );

        PhpCodeWriter subject = new(
            null!, //new Configuration(),
            new ClassList(),
            enums,
            null!, //new Wsdl(),
            null! // ClientClass clientClass
            );

        subject.GeneratePhpEnums(fileBuilder);

        classBuilder.Received().AddConstant(constName, constValue);
    }
}
