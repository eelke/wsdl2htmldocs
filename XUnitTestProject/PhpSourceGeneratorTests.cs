﻿using System;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wsdl2htmldocs.phpgenerator.phpsourcegenerator;
using XUnitTestProject.Util;

namespace XUnitTestProject;

public class PhpSourceGeneratorTests
{
    private FakeFile testFile = new();


    [Fact]
    public void MockFileSystemWorksAsExpected()
    {

        using (var stream = testFile.Writer)
        {
            stream.WriteLine("Hello");
        }

        Assert.Contains("Hello", testFile.TextContents);
    }

    [Fact]
    public void FileHasNamespace()
    {
        using (var writer = testFile.Writer)
        {

            FileBuilder builder = new(writer, "NS");
            builder.Class("Test", b => { });
        }
        Assert.Contains("namespace NS;", testFile.TextContents);
    }

    [Fact]
    public void ClassWithoutBase()
    {
        using (var writer = testFile.Writer)
        {

            FileBuilder builder = new(writer);
            builder.Class("Test", b => { });
        }

        Assert.Contains("class Test {", testFile.TextContents);
        Assert.Contains("}", testFile.TextContents);
    }


    [Fact]
    public void AddConstant()
    {
        using (var writer = testFile.Writer)
        {
            FileBuilder builder = new(writer);
            builder.Class("Test", cls =>
            {
                cls.AddConstant("foo", "bar");
            });
        }

        Assert.Contains("const foo = 'bar';", testFile.TextContents);
    }
}
