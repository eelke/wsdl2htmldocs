﻿using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions.TestingHelpers;

namespace XUnitTestProject.Util;

public class FakeFile
{
    const string fileName = @"c:\test.txt";

    private readonly MockFileData fileContents = new("");
    private readonly MockFileSystem fileSystem;

    public TextWriter Writer => new StreamWriter(fileSystem.FileStream.New(fileName, FileMode.OpenOrCreate));

    public string TextContents => fileContents.TextContents;

    public FakeFile()
    {
        fileSystem = new(new Dictionary<string, MockFileData>
        {
            { fileName, fileContents },
        });
    }
}
