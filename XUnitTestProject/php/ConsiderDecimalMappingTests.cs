﻿using System.Data.SqlTypes;
using System.Xml.Linq;
using wsdl2htmldocs.phpgenerator;
using wsdl2htmldocs.sidecar;
using wsdl2htmldocs.soap;

namespace XUnitTestProject.php;

public class ConsiderDecimalMappingTests
{
    [Theory]
    [InlineData("SalesPrice")]
    [InlineData("QuantityInStock")]
    [InlineData("AmountDue")]
    public void NameRecognized(string name)
    {
        Element elem = new(new(name, "ns1"))
        {
            Type = new("long", "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(elem, [elem], "foo");
        Assert.NotNull(result);
    }

    [Theory]
    [InlineData("int")]
    [InlineData("decimal")]
    [InlineData("date")]
    [InlineData("string")]
    [InlineData("boolean")]
    public void OnlyForTypeLong(string typname)
    {
        Element elem = new(new("price", "ns1"))
        {
            Type = new(typname, "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(elem, [elem], "foo");
        Assert.Null(result);
    }

    [Theory]
    [InlineData("Sales")]
    [InlineData("InStock")]
    [InlineData("Due")]
    public void NameNotRecognized(string name)
    {
        Element elem = new(new(name, "ns1"))
        {
            Type = new("long", "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(elem, [elem], "foo");
        Assert.Null(result);
    }

    [Theory]
    [InlineData("decimalPlaces")]
    [InlineData("SalesPriceDecimalPlaces")]
    [InlineData("decimalPlacesSalesPrice")]
    public void WithDecimalPlaces(string decimalsName)
    {
        Element valueElem = new(new("SalesPrice", "ns1"))
        {
            Type = new("long", "http://www.w3.org/2001/XMLSchema")
        };
        Element decimalsElem = new(new(decimalsName, "ns1"))
        {
            Type = new("int", "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(valueElem, [valueElem, decimalsElem], "foo");
        Assert.NotNull(result);
        Assert.NotNull(result.Scale);
    }

    [Theory]
    [InlineData("OtherField")]
    [InlineData("decimalPlacesOther")]
    public void NoDecimalPlacesWHenWrongName(string decimalsName)
    {
        Element valueElem = new(new("SalesPrice", "ns1"))
        {
            Type = new("long", "http://www.w3.org/2001/XMLSchema")
        };
        Element decimalsElem = new(new(decimalsName, "ns1"))
        {
            Type = new("int", "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(valueElem, [valueElem, decimalsElem], "foo");
        Assert.NotNull(result);
        Assert.Null(result.Scale);
    }

    [Fact]
    public void NoDecimalPlacesWhenWrongType()
    {
        Element valueElem = new(new("SalesPrice", "ns1"))
        {
            Type = new("long", "http://www.w3.org/2001/XMLSchema")
        };
        Element decimalsElem = new(new("decimalPlaces", "ns1"))
        {
            Type = new("boolean", "http://www.w3.org/2001/XMLSchema")
        };
        Mapping? result = GeneratePhp.ConsiderDecimalMapping(valueElem, [valueElem, decimalsElem], "foo");
        Assert.NotNull(result);
        Assert.Null(result.Scale);
    }
}
