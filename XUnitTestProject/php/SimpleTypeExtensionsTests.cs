﻿using System;
using wsdl2htmldocs.phpgenerator;
using wsdl2htmldocs.phpgenerator.config;
using wsdl2htmldocs.phpgenerator.ir;
using wsdl2htmldocs.sidecar;
using wsdl2htmldocs.soap;

namespace XUnitTestProject.php;

public class SimpleTypeExtensionsTests
{
    private readonly PhpGeneratorConfiguration configuration = new(
        new GeneratePhpParams()
        {
            AutoDecimalMappings = false,
        },
        new WsdlSidecar(),
        null);

    [Fact]
    public void ToIrEnum()
    {
        SimpleType input = new(new("foo", "ns1"))
        {
            Documentation = "docs",
            Restriction = new()
            {
                Base = new("string", "xsd"),
                Enumerations = [
                        new() { Value = "a" },
                        new() { Value = "b" },
                    ]
            }
        };

        IrEnum result = input.ToIrEnum(configuration);

        Assert.Equal(input.Name!.Name, result.WsdlName);
        Assert.Equal(input.Name!.Name, result.CodeName);
        Assert.Equal(2, result.Values.Count);
        Assert.Single(result.Values, v => v.Name == "A" && v.Value == "a");
        Assert.Single(result.Values, v => v.Name == "B" && v.Value == "b");
    }

    [Fact]
    public void ToIrEnum_RemovesTypenamePrefix()
    {
        SimpleType input = new(new("foo", "ns1"))
        {
            Documentation = "docs",
            Restriction = new()
            {
                Base = new("string", "xsd"),
                Enumerations = [
                        new() { Value = "foo_a" },
                        new() { Value = "foo_b" },
                    ]
            }
        };

        IrEnum result = input.ToIrEnum(configuration);

        Assert.Equal(2, result.Values.Count);
        Assert.Single(result.Values, v => v.Name == "A" && v.Value == "foo_a");
        Assert.Single(result.Values, v => v.Name == "B" && v.Value == "foo_b");
    }

    [Fact]
    public void ToIrEnum_ExpectsRestriction()
    {
        SimpleType input = new(new("foo", "ns1"))
        {
            Documentation = "docs",
            Restriction = null
        };

        Assert.Throws<ArgumentException>(() => input.ToIrEnum(configuration));
    }
}
