﻿using wsdl2htmldocs.parser;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs;

public static class CommandHandlerUtil
{
    public static Wsdl LoadWsdls(List<System.IO.FileInfo> wsdls)
    {
        Wsdl merged = new();
        foreach (var wsdl in wsdls)
        {
            var parser = new WsdlDocParser();
            var data = parser.Parse(wsdl.FullName);
            merged.Merge(data);
        }
        return merged;
    }
}