﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using wsdl2htmldocs.htmlgenerator;
using wsdl2htmldocs.merge;
using wsdl2htmldocs.phpgenerator;

namespace wsdl2htmldocs;

public class Program
{

    static int Main(string[] args)
    {
        return CreateRoot().InvokeAsync(args).Result;
    }

    static public Command CreateRoot(
        IGeneratePhpHandler? generatePhpHandler = null,
        IHtmlDocsHandler? htmlDocsHandler = null,
        IMergeCommandHandler? mergeHandler = null)
    {
        var codeCommand = new Command("code")
        {
            CreateGeneratePhpCommand(generatePhpHandler)
        };

        var docsCommand = new Command("docs")
        {
            CreateHtmlDocsCommand(htmlDocsHandler)
        };

        // Create a root command with some options
        var rootCommand = new RootCommand
        {
            new Argument<List<FileInfo>>("wsdls", "The wsdl files for which documentation should be generated")
            {
                Arity = ArgumentArity.OneOrMore
            }
        };

        rootCommand.Add(codeCommand);
        rootCommand.Add(docsCommand);
        rootCommand.Add(CreateMergeCommand(mergeHandler));

        rootCommand.Description = "Program that reads one or more WSDL files and generates HTML documentation";
        return rootCommand;
    }

    static public Command CreateGeneratePhpCommand(IGeneratePhpHandler? handler = null)
    {
        handler ??= new GeneratePhpHandler();

        var phpCommand = new Command("php")
        {
           new Option<string>("--output",
               description: "Defaults to the same path and name as the wsdl but with a .php extension"),
           new Option<DirectoryInfo>("--output-basepath",
               description: @"Defaults to the same path as the wsdl. If this path is relative 
it will be relative to the working directory, all other relative output paths will be relative to this"),
           new Option<bool>("--auto-decimal-mappings",
                description: @"Search for long fields that are candidates for conversion to Decimal"),
           new Option<bool>("--remove-list-wrappers",
                description: @"A list wrapper is when a ComplexType only has a single repeating value, this option flattens the structure")
        };
        phpCommand.Handler = CommandHandler.Create<GeneratePhpParams>(config =>
        {
            handler.Handle(config);
        });

        return phpCommand;
    }

    public static Command CreateHtmlDocsCommand(IHtmlDocsHandler? handler = null)
    {
        handler ??= new HtmlCommandHandler();

        var htmlCommand = new Command("html")
        {
            new Option<FileInfo>(
                "--custom-css"),
            new Option<bool>(
                "--link-custom-css",
                getDefaultValue: () => false,
                description: "Set to true to link to the custom css instead of embedding it"),
            new Option<bool>(
                "--list-operations",
                getDefaultValue: () => true,
                description: "Set to false when you do not want a listing of all operations at the start of the document"),
            new Option<bool>(
                "--list-types",
                getDefaultValue: () => false,
                description: "Set to true when you do want a listing of all types at the start of the document"),
            new Option<int>(
                "--max-depth",
                getDefaultValue: () => 3,
                description: "Limit how deep the display of nested structures goes, use -1 for no limit (it will stop when cyclic chain of types is detected)"),
            new Option<FileInfo>(
                "--output",
                description: "Defaults to the same path and name as the wsdl but with an .html extension"),
            new Option<bool>(
                "--type-chapters",
                getDefaultValue: () => true,
                "Enables generation of a chapter per type, when you disable this you might want to set max depth to -1 to make sure everything is included in the operations"),
        };

        htmlCommand.Handler = CommandHandler.Create<GenerateHtmlConfig>(
            config => handler.Handle(config)
            );
        return htmlCommand;
    }

    public static Command CreateMergeCommand(IMergeCommandHandler? handler = null)
    {
        handler ??= new MergeCommandHandler();

        var command = new Command("merge")
        {
            new Option<FileInfo>(
                "--output",
                description: "Defaults to the same path and name as the first wsdl but with -merged concatenated to the base filename"),
        };
        command.Handler = CommandHandler.Create<MergeWsdlConfig>(
            config =>
            {
                handler.Handle(config);
            });
        return command;
    }
}
