﻿using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Abstractions;
using System.Xml;
using wsdl2htmldocs.soap;
using Type = wsdl2htmldocs.soap.Type;

namespace wsdl2htmldocs.htmlgenerator;

public class GenerateHtml
{
    private readonly Wsdl wsdl;
    private readonly GenerateHtmlConfig config;
    private readonly IFileSystem fileSystem;

    public GenerateHtml(Wsdl wsdl, GenerateHtmlConfig? config = null, IFileSystem? fileSystem = null)
    {
        this.wsdl = wsdl;
        this.config = config ?? new GenerateHtmlConfig();
        this.fileSystem = fileSystem ?? new FileSystem();
    }

    private static string MakeMultiplicityString(int minOccurs, int maxOccurs)
    {
        Contract.Requires(minOccurs >= 0);
        Contract.Requires(minOccurs <= maxOccurs || maxOccurs == -1);
        Contract.Requires(maxOccurs == -1 || maxOccurs > 0);

        string multiplicity; ;
        if (maxOccurs == -1)
        {
            if (minOccurs == 0)
            {
                multiplicity = "*";
            }
            else if (minOccurs == 1)
            {
                multiplicity = "+";
            }
            else
            {
                multiplicity = $"{{$elem.MinOccurs,}}";
            }
        }
        else if (maxOccurs == 1)
        {
            if (minOccurs == 1)
            {
                multiplicity = string.Empty;
            }
            else
            {
                // min must be 0
                multiplicity = "?";
            }
        }
        else
        {
            // maxOccurs should be > 1
            if (minOccurs == 0)
            {
                multiplicity = $"{{,$elem.MaxOccurs}}";
            }
            else if (minOccurs == maxOccurs)
            {
                multiplicity = $"{{$elem.MinOccurs}}";
            }
            else
            {
                multiplicity = $"{{$elem.MinOccurs, $elem.MaxOccurs}}";
            }
        }
        return multiplicity;
    }

    public void Generate(Stream stream)
    {
        XmlWriterSettings settings = new XmlWriterSettings()
        {
            Indent = true
        };

        using var writer = XmlWriter.Create(stream, settings);

        writer.WriteStartDocument();
        writer.WriteStartElement("html");
        WriteHead(writer);
        WriteBody(writer);
        writer.WriteEndElement();
        writer.WriteEndDocument();
    }

    void WriteHead(XmlWriter writer)
    {
        writer.WriteStartElement("head");
        if (wsdl.Service != null)
            writer.WriteElementString("title", wsdl.Service.Name + " documentation");
        WriteStyleSheet(writer);
        writer.WriteEndElement();
    }

    void WriteBuildinCSS(XmlWriter writer)
    {
        writer.WriteString(
@"
body {
	margin: 0;
	padding: 0.5em;
	height: 100%;
	height: auto;
	color: black;
	background-color: white;
	font: normal 100%/120% 'Open Sans', sans-serif;
}

h1, h2, h3 {
	margin: 10px 10px 2px;
	font-family: 'Open Sans', sans-serif;
	font-weight: normal;
}

h1 {
	font-weight: bold;
	letter-spacing: 3px;
	font-size: 220%;
	line-height: 100%;
}

h2 {
	font-weight: bold;
	font-size: 175%;
	line-height: 200%;
}

h3 {
	font-size: 150%;
	line-height: 150%;
}

span.type {
    color: blue;
}

div.part {
    background-color: rgb(233, 239, 247);
    padding: 1em;
    margin: 1em;
}

div.type {
    background-color: rgb(233, 239, 247);
    margin: 1em;
}

div.type {
    padding: 0.2em 0;
}

.documentation {
    font-size: 80%;
    color: green;
}

.serviceName {
    padding: 1em 0 0.5em 0;
    font-size: 300%;
    font-weight: bold;
}

.serviceName:before {
    content: ""WEB SERVICE: "";
    font-size: 50%;
}

.targetNamespace {
    font-size: 200%;
    font-weight: bold;
}

.targetNamespace:before {
    content: ""target namespace: "";
    font-size: 50%;
}

div.operationChapter h2:before {
    content: ""operation: "";
}

div.inputName:before {
    content: ""input: "";
    font-size: 85%;
    color: rgb(80, 80, 80);
}

div.outputName:before {
    content: ""output: "";
    font-size: 85%;
    color: rgb(80, 80, 80);
}

div.service documentation {
    font-size: 125%;
}
");
    }

    void WriteStyleSheet(XmlWriter writer)
    {
        if (config.LinkCustomCss && config.CustomCss != null)
        {
            writer.WriteStartElement("link");
            writer.WriteAttributeString("rel", "stylesheet");
            writer.WriteAttributeString("type", "text/css");
            writer.WriteAttributeString("href",
                fileSystem.Path.GetRelativePath(config.Output.DirectoryName!, config.CustomCss.FullName));
            writer.WriteEndElement();
        }
        else
        {
            writer.WriteStartElement("style");
            writer.WriteAttributeString("type", "text/css");
            if (config.CustomCss == null)
            {
                WriteBuildinCSS(writer);
            }
            else
            {
                writer.WriteString(
                    fileSystem.File.ReadAllText(config.CustomCss.FullName));
            }
            writer.WriteEndElement();
        }
    }

    void WriteBody(XmlWriter writer)
    {
        writer.WriteStartElement("body");

        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "service");

        if (!string.IsNullOrWhiteSpace(wsdl.Service?.Name))
        {
            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "serviceName");
            writer.WriteString(wsdl.Service.Name);
            writer.WriteEndElement();
        }

        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "targetNamespace");
        writer.WriteString(wsdl.TargetNamespace);
        writer.WriteEndElement();

        if (!string.IsNullOrWhiteSpace(wsdl.Service?.Documentation))
        {
            writer.WriteStartElement("p");
            writer.WriteAttributeString("class", "documentation");
            writer.WriteString(wsdl.Service.Documentation);
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        if (config.ListOperations || config.ListTypes)
        {
            writer.WriteElementString("h1", "Contents");
            if (config.ListOperations)
                WriteOperationsList(writer);
            if (config.ListTypes)
                WriteTypesList(writer);
        }

        writeOperationChapters(writer);
        if (config.TypeChapters)
        {
            WriteTypeChapters(writer);
        }

        writer.WriteStartElement("div");
        writer.WriteAttributeString("id", "footer");
        writer.WriteString("This page was generated by wsdl2htmldocs (");
        writer.WriteStartElement("a");
        writer.WriteAttributeString("href", "https://gitlab.com/eelke/wsdl2htmldocs");
        writer.WriteString("https://gitlab.com/eelke/wsdl2htmldocs");
        writer.WriteEndElement();
        writer.WriteString(")");
        writer.WriteEndElement();

        writer.WriteEndElement();
    }

    private void WriteOperationsList(XmlWriter writer)
    {
        var sl = new SortedDictionary<string, Operation>();
        foreach (var op in wsdl.PortType.Operations)
        {
            var s = op.Name.Name;
            for (int i = 0; i < s.Length; ++i)
            {
                if (Char.IsUpper(s[i]))
                {
                    s = string.Concat(s.AsSpan(i), ".", s.AsSpan(0, i));
                }
            }
            sl.Add(s, op);
        }


        writer.WriteElementString("h2", "Operations");

        writer.WriteStartElement("ul");
        writer.WriteAttributeString("class", "operationList");
        //foreach (var op in _wsdl.Operations)
        foreach (var e in sl)
        {
            var op = e.Value;
            writer.WriteStartElement("li");
            writer.WriteStartElement("a");
            writer.WriteAttributeString("href", "#" + OperationRefId(op));
            writer.WriteString(op.Name.Name);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    private void WriteTypesList(XmlWriter writer)
    {
        writer.WriteElementString("h2", "Types");

        writer.WriteStartElement("ul");
        writer.WriteAttributeString("class", "typeNamespaces");
        foreach (var ns in wsdl.Types.Namespaces())
        {
            var types = wsdl.Types.Types(ns);
            writer.WriteStartElement("li");
            writer.WriteElementString("p", ns);
            writer.WriteStartElement("ul");
            foreach (var s in types.Where(t => t.Name is { }))
            {
                writer.WriteStartElement("li");

                writer.WriteStartElement("a");
                writer.WriteAttributeString("href", "#" + TypeRefId(s));
                writer.WriteString(s.Name!.Name);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    private void writeOperationChapters(XmlWriter writer)
    {
        writer.WriteElementString("h1", "Operations");
        foreach (var op in wsdl.PortType.Operations)
        {
            WriteOperationChapter(writer, op);
        }

    }

    private void WriteOperationChapter(XmlWriter writer, Operation op)
    {
        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "operationChapter");

        writer.WriteStartElement("h2");
        writer.WriteStartElement("a");
        writer.WriteAttributeString("id", OperationRefId(op));
        writer.WriteString(op.Name.Name);
        writer.WriteEndElement();
        writer.WriteEndElement(); // h2

        if (!string.IsNullOrWhiteSpace(op.Documentation))
        {
            writer.WriteElementString("h3", "Description");
            writer.WriteStartElement("p");
            writer.WriteAttributeString("class", "documentation");
            writer.WriteString(op.Documentation);
            writer.WriteEndElement();
        }
        if (op.Input != null)
        {
            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "input io");

            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "inputName ioName");
            writer.WriteElementString("span", op.Input.Name);
            writer.WriteEndElement(); // div.inputName

            if (wsdl.Messages.TryGetValue(op.Input, out Message? msg))
            {
                WriteMessageParts(writer, msg);
            }
            else
            {
                throw new ApplicationException("Missing message");
            }

            writer.WriteEndElement(); // div.input
        }
        if (op.Output != null)
        {
            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "output io");

            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "outputName ioName");
            writer.WriteElementString("span", op.Output.Name);
            writer.WriteEndElement(); // div.inputName

            if (wsdl.Messages.TryGetValue(op.Output, out Message? msg))
            {
                WriteMessageParts(writer, msg);
            }
            else
            {
                throw new ApplicationException("Missing message");
            }

            writer.WriteEndElement(); // div.output

        }
        writer.WriteEndElement(); // div
    }

    private void WriteMessageParts(XmlWriter writer, Message msg)
    {
        foreach (var p in msg.Parts)
        {
            writer.WriteStartElement("div");
            writer.WriteAttributeString("class", "part");

            writer.WriteStartElement("span");
            writer.WriteAttributeString("class", "name");
            writer.WriteString(p.Name.Name);
            writer.WriteEndElement();


            if (p.Element != null)
            {
                writer.WriteString(" ");
                writer.WriteStartElement("span");
                writer.WriteAttributeString("class", "element");
                writer.WriteString(p.Element.Name);
                writer.WriteEndElement();

                var r = wsdl.Elements.Find(e => e.Name == p.Element);
                if (r != null)
                {
                    if (r.AnonymousType != null)
                    {
                        WriteTypeDetails(writer, r.AnonymousType);
                    }
                    else if (r.Type != null)
                    {
                        var t = wsdl.Types.Find(r.Type);
                        if (t != null)
                        {
                            WriteTypeDetails(writer, t);
                        }
                    }
                }
            }
            if (p.Type != null)
            {
                var t = wsdl.Types.Find(p.Type);
                if (t != null)
                {
                    WriteTypeDetails(writer, t);
                }
            }

            writer.WriteEndElement();
        }
    }

    private string OperationRefId(Operation op)
    {
        return "opRef_" + op.Name;
    }

    private void WriteTypeChapters(XmlWriter writer)
    {
        writer.WriteElementString("h1", "Types");
        foreach (var ns in wsdl.Types.Namespaces())
        {
            foreach (var type in wsdl.Types.Types(ns))
            {
                WriteTypeChapter(writer, type);
            }
        }
    }

    private void WriteTypeChapter(XmlWriter writer, Type type)
    {
        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "typeChapter");

        writer.WriteStartElement("h2");
        writer.WriteStartElement("a");
        writer.WriteAttributeString("id", TypeRefId(type));
        writer.WriteString(type.Name?.Name ?? "<anonymous>");
        writer.WriteEndElement(); // a
        writer.WriteEndElement(); // h2

        WriteTypeDetails(writer, type);

        writer.WriteEndElement(); // div
    }

    private void WriteTypeDetails(XmlWriter writer, Type type)
    {
        var complex = type as ComplexType;
        if (complex != null)
        {
            WriteComplexTypeDetails(writer, complex);
        }
        else
        {
            var simple = type as SimpleType;
            if (simple != null)
            {
                WriteSimpleTypeDetails(writer, simple);
            }
        }
    }

    private void WriteSimpleTypeDetails(XmlWriter writer, SimpleType simple)
    {
        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "simple type");

        if (!string.IsNullOrWhiteSpace(simple.Documentation))
        {
            writer.WriteStartElement("p");
            writer.WriteAttributeString("class", "documentation");
            writer.WriteString(simple.Documentation);
            writer.WriteEndElement();
        }

        if (simple.Restriction.Enumerations.Count > 0)
        {
            writer.WriteElementString("h3", "Enumeration");
            writer.WriteStartElement("ul");
            foreach (var e in simple.Restriction.Enumerations)
            {
                writer.WriteStartElement("li");

                writer.WriteString(e.Value);

                if (!string.IsNullOrWhiteSpace(e.Documentation))
                {
                    writer.WriteStartElement("div");
                    writer.WriteAttributeString("class", "documentation");
                    writer.WriteString(e.Documentation);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    private void WriteComplexTypeDetails(XmlWriter writer, ComplexType complex)
    {
        writer.WriteStartElement("div");
        writer.WriteAttributeString("class", "complex type");

        if (!string.IsNullOrWhiteSpace(complex.Documentation))
        {
            writer.WriteStartElement("p");
            writer.WriteAttributeString("class", "documentation");
            writer.WriteString(complex.Documentation);
            writer.WriteEndElement();
        }

        var baseType = complex.GetBaseType(wsdl);
        if (baseType != null)
        {
            writer.WriteStartElement("p");
            writer.WriteAttributeString("class", "inherits");
            writer.WriteString("inherits: ");
            writer.WriteStartElement("a");
            writer.WriteAttributeString("href", TypeRefUrl(baseType));
            writer.WriteStartElement("span");
            writer.WriteAttributeString("class", "type");
            writer.WriteString(baseType.Name.Name);
            writer.WriteEndElement(); // span

            writer.WriteEndElement(); // a base class link
            writer.WriteEndElement(); // p
        }

        writer.WriteStartElement("ul");
        writer.WriteAttributeString("class", "complex");
        WriteElementsRecursively(writer, complex);
        writer.WriteEndElement(); // ul

        writer.WriteEndElement(); // div class complex type
    }

    private void WriteElementsRecursively(XmlWriter writer, ComplexType ct, HashSet<Type>? recursion_protection = null, int current_depth = 0)
    {
        if (recursion_protection == null)
        {
            recursion_protection = new HashSet<Type>();
        }
        recursion_protection.Add(ct);
        var baseType = ct.GetBaseType(wsdl);
        if (baseType != null)
        {
            WriteElementsRecursively(writer, baseType, recursion_protection, current_depth);
        }
        WriteProperties(writer, ct, recursion_protection, current_depth);
        recursion_protection.Remove(ct);
    }

    private void WriteProperties(XmlWriter writer, ComplexType ct, HashSet<Type> recursion_protection, int current_depth)
    {
        foreach (var elem2 in ct.Elements)
        {
            var elem = wsdl.followReferences(elem2);
            writer.WriteStartElement("li");
            writer.WriteAttributeString("class", "element");
            writer.WriteString(elem.Name?.Name ?? "<anonymous>");

            writer.WriteString(" ");
            var elem_type = wsdl.Types.Find(elem.Type);
            if (elem_type != null)
            {
                writer.WriteStartElement("a");
                writer.WriteAttributeString("href", TypeRefUrl(elem_type));
            }
            writer.WriteStartElement("span");
            writer.WriteAttributeString("class", "type");
            writer.WriteString(elem.Type!.Name);
            writer.WriteEndElement(); // span
            if (elem_type != null)
            {
                writer.WriteEndElement(); // a details link
            }

            writer.WriteString(" ");

            writer.WriteStartElement("span");
            writer.WriteAttributeString("class", "multiplicity");
            writer.WriteString(MakeMultiplicityString(elem2.MinOccurs, elem2.MaxOccurs));
            writer.WriteEndElement(); // span

            if (!string.IsNullOrWhiteSpace(elem.Documentation))
            {
                writer.WriteStartElement("div");
                writer.WriteAttributeString("class", "documentation");
                writer.WriteString(elem.Documentation);
                writer.WriteEndElement();
            }

            if (elem_type != null)
            {
                if (config.MaxDepth == -1 || current_depth < config.MaxDepth)
                {
                    var nested_ct = elem_type as ComplexType;
                    if (nested_ct != null)
                    {
                        if (!recursion_protection.Contains(nested_ct))
                        {
                            writer.WriteStartElement("ul");
                            WriteElementsRecursively(writer, nested_ct, recursion_protection, current_depth + 1);
                            writer.WriteEndElement();
                        }
                        else
                        {
                            writer.WriteString("(recursive)");
                        }
                    }
                }
                else
                {
                    writer.WriteString(". . .");
                }

            }

            writer.WriteEndElement(); // li
        }
    }

    private string TypeRefId(Type type)
    {
        return "typeRef_" + type.Name!.Name;
    }

    /// <summary>
    /// Retourneerd de URL voor gebruik in hyperlink.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private string TypeRefUrl(Type type)
    {
        return "#" + TypeRefId(type);
    }

}
