﻿using System.IO;

namespace wsdl2htmldocs.htmlgenerator;

public class GenerateHtmlConfig
{
    private FileInfo? output;

    public bool ListOperations { get; set; } = true;
    public bool ListTypes { get; set; } = false;
    public int MaxDepth { get; set; } = 3;
    public bool TypeChapters { get; set; } = true;
    public FileInfo? CustomCss { get; set; } = null;
    public bool LinkCustomCss { get; set; } = false;
    public FileInfo Output
    {
        get
        {
            output ??= new FileInfo(Path.ChangeExtension(Wsdls[0].FullName, "html"));
            return output;
        }
        set => output = value;
    }
    public List<FileInfo> Wsdls { get; set; } = new();
}
