﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace wsdl2htmldocs.htmlgenerator;



public class HtmlCommandHandler : IHtmlDocsHandler
{
    public void Handle(GenerateHtmlConfig config)
    {
        var wsdl = CommandHandlerUtil.LoadWsdls(config.Wsdls);

        var generator = new GenerateHtml(wsdl, config);
        generator.Generate(config.Output.Create());

        Console.Out.WriteLine("HTML docs generated!");
    }
}
