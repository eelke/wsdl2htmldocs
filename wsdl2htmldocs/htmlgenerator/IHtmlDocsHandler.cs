﻿
namespace wsdl2htmldocs.htmlgenerator;

public interface IHtmlDocsHandler
{
    void Handle(GenerateHtmlConfig config);
}