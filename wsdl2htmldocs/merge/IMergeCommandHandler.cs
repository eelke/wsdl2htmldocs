﻿namespace wsdl2htmldocs.merge;

public interface IMergeCommandHandler
{
    void Handle(MergeWsdlConfig config);
}