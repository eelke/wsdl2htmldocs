﻿using System.IO;
using System.Xml;

namespace wsdl2htmldocs.merge;

public class MergeCommandHandler : IMergeCommandHandler
{
    public void Handle(MergeWsdlConfig config)
    {
        var wsdl = CommandHandlerUtil.LoadWsdls(config.Wsdls);

        WriteWsdl(config, wsdl);
        Console.Out.WriteLine("Merged WSDL written!");
    }

    private void WriteWsdl(MergeWsdlConfig config, soap.Wsdl wsdl)
    {
        var settings = new XmlWriterSettings()
        {
            Async = false,
            Indent = true,
            OmitXmlDeclaration = false
        };

        var w = new WsdlWriter(wsdl, config.Output.Create());
        w.Invoke();
    }

}
