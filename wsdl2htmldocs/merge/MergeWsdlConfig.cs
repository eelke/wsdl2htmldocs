﻿using System.IO;

namespace wsdl2htmldocs.merge;

public class MergeWsdlConfig
{
    private FileInfo? output = null;

    public List<FileInfo> Wsdls { get; set; } = new();
    public FileInfo Output
    {
        get
        {
            output ??= new FileInfo("merged.wsdl");
            return output;
        }
        set => output = value;
    }
}