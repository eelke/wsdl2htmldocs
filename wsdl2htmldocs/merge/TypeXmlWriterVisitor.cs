﻿using System.Xml;
using wsdl2htmldocs.soap;
#nullable enable

namespace wsdl2htmldocs.merge;

internal class TypeXmlWriterVisitor : ITypeVisitor
{
    private XmlWriter writer;
    private Wsdl wsdl;

    public TypeXmlWriterVisitor(XmlWriter writer, Wsdl wsdl)
    {
        this.writer = writer;
        this.wsdl = wsdl;
    }

    public void visitComplexType(ComplexType ct)
    {
        writer.WriteStartElement("complexType");
        if (ct.Name != null)
            writer.WriteAttributeString("name", ct.Name.Name);
        WriteCommonBaseType(ct);
        if (ct.BaseTypeName != null)
        {
            writer.WriteStartElement("complexContent");
            writer.WriteStartElement("extension");
            writer.WriteAttributeString("base", wsdl.PrefixName(ct.BaseTypeName));
            WriteSequence(ct.Elements);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            WriteSequence(ct.Elements);
        }
        writer.WriteEndElement();
    }

    private void WriteSequence(List<Element> elements)
    {
        writer.WriteStartElement("sequence");
        foreach (Element element in elements)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("name", element.Name?.Name);
            writer.WriteAttributeString("type", wsdl.PrefixName(element.Type!));

            if (element.MinOccurs != 1)
                writer.WriteAttributeString("minOccurs", $"{element.MinOccurs}");

            if (element.MaxOccurs != 1)
                writer.WriteAttributeString("maxOccurs", element.MaxOccurs == -1 ? "unbounded" : $"{element.MaxOccurs}");

            WriteAnnotationDocumentation(element.Documentation);
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    public void visitSimpleType(SimpleType st)
    {
        writer.WriteStartElement("simpleType");
        writer.WriteAttributeString("name", st.Name?.Name);
        WriteCommonBaseType(st);
        WriteRestriction(st.Restriction!);

        writer.WriteEndElement();
    }

    private void WriteRestriction(Restriction restriction)
    {
        if (restriction.Enumerations.Count == 0)
            return;

        writer.WriteStartElement("restriction");
        writer.WriteAttributeString("base", wsdl.PrefixName(restriction.Base));

        foreach (var r in restriction.Enumerations)
        {
            writer.WriteStartElement("enumeration");
            writer.WriteAttributeString("value", r.Value);
            WriteAnnotationDocumentation(r.Documentation);
            writer.WriteEndElement();
        }

        writer.WriteEndElement();
    }

    private void WriteCommonBaseType(soap.Type t)
    {
        WriteAnnotationDocumentation(t.Documentation);
    }

    private void WriteAnnotationDocumentation(string? documentation)
    {
        if (string.IsNullOrEmpty(documentation))
            return;

        writer.WriteStartElement("annotation");
        writer.WriteElementString("documentation", documentation);
        writer.WriteEndElement();
    }
}