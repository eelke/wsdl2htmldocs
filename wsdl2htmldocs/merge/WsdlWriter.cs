﻿using System.IO;
using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.merge;

internal class WsdlWriter
{
	private readonly XmlWriterSettings xmlSettings;
	private readonly Wsdl wsdl;
	private readonly XmlWriter writer;

	private readonly string soapNsPrefix;

	public WsdlWriter(Wsdl wsdl, Stream stream)
	{
		this.wsdl = wsdl;

		soapNsPrefix = wsdl.GetNamespacePrefix("http://schemas.xmlsoap.org/wsdl/soap/");

		xmlSettings = new XmlWriterSettings()
		{
			Async = false,
			Indent = true,
			OmitXmlDeclaration = false
		};

		writer = XmlWriter.Create(stream, xmlSettings);
	}

	public void Invoke()
	{
		writer.WriteStartElement(null, @"definitions", @"http://schemas.xmlsoap.org/wsdl/");
		if (!string.IsNullOrEmpty(wsdl.Name))
			writer.WriteAttributeString(null, @"name", null, wsdl.Name);
		writer.WriteAttributeString(null, @"targetNamespace", null, wsdl.TargetNamespace);
		foreach (var ns in wsdl.NamespaceAliasses)
			writer.WriteAttributeString("xmlns", ns.Key, null, ns.Value);

		WriteTypes();
		WriteMessages();
		WritePortType();
		WriteBindings();
		WriteService();

		writer.WriteEndElement();
		writer.Flush();

	}

	private void WriteService()
	{
		writer.WriteStartElement("service");
		writer.WriteAttributeString("name", wsdl.Service.Name);

		if (!string.IsNullOrWhiteSpace(wsdl.Service.Documentation))
			writer.WriteElementString("documentation", wsdl.Service.Documentation);

		WriteServicePort();

		writer.WriteEndElement();
	}

	private void WriteServicePort()
	{
		var port = wsdl.Service.Port;

		writer.WriteStartElement("port");
		writer.WriteAttributeString("name", port.Name);
		if (port.Binding != null)
			writer.WriteAttributeString("binding", wsdl.PrefixName(port.Binding));

		writer.WriteStartElement(soapNsPrefix, "address", null);
		writer.WriteAttributeString("location", port.AddressLocation);
		writer.WriteEndElement();

		writer.WriteEndElement();
	}

	private void WriteBindings()
	{
		writer.WriteStartElement("binding");
		writer.WriteAttributeString("name", wsdl.Binding.Name);
		if (wsdl.Binding.Type != null)
			writer.WriteAttributeString("type", wsdl.PrefixName(wsdl.Binding.Type));

		writer.WriteStartElement(soapNsPrefix, "binding", null);
		writer.WriteAttributeString("style", "document");
		writer.WriteAttributeString("transport", "http://schemas.xmlsoap.org/soap/http");
		writer.WriteEndElement();

		foreach (var op in wsdl.PortType.Operations)
			WriteOperationBinding(op);

		writer.WriteEndElement();
	}

	private void WriteOperationBinding(Operation op)
	{
		writer.WriteStartElement("operation");
		writer.WriteAttributeString("name", op.Name.Name);

		writer.WriteStartElement(soapNsPrefix, "operation", null);
		writer.WriteAttributeString("soapAction", string.Empty);
		writer.WriteEndElement();

		WriteIOBinding("input");
		WriteIOBinding("output");

		writer.WriteEndElement();
	}

	private void WriteIOBinding(string tag)
	{
		writer.WriteStartElement(tag);

		writer.WriteStartElement("SOAP", "body", null);
		writer.WriteAttributeString("use", "literal");
		writer.WriteAttributeString("parts", "Body");
		writer.WriteEndElement();

		writer.WriteEndElement();
	}

	private void WritePortType()
	{
		writer.WriteStartElement("portType");
		writer.WriteAttributeString("name", wsdl.PortType.Name);

		WriteOperations(wsdl.PortType.Operations);

		writer.WriteEndElement();
	}

	private void WriteOperations(List<Operation> operations)
	{
		foreach (var operation in operations)
		{
			WriteOperation(operation);
		}
	}

	private void WriteOperation(Operation operation)
	{
		writer.WriteStartElement("operation");
		writer.WriteAttributeString("name", operation.Name.Name);
		if (!string.IsNullOrEmpty(operation.Documentation))
			writer.WriteElementString("documentation", operation.Documentation);
		if (operation.Input != null)
		{
			WriteOperationMessage("input", operation.Input);
		}
		if (operation.Output != null)
		{
			WriteOperationMessage("output", operation.Output);
		}
		writer.WriteEndElement();
	}

	private void WriteOperationMessage(string tag, XmlQualifiedName message)
	{
		writer.WriteStartElement(tag);
		writer.WriteAttributeString("message", wsdl.PrefixName(message));
		writer.WriteEndElement();
	}

	private void WriteMessages()
	{
		foreach (var message in wsdl.Messages)
			WriteMessage(message.Value);
	}

	private void WriteMessage(Message msg)
	{
		writer.WriteStartElement("message");
		writer.WriteAttributeString("name", msg.Name.Name);

		foreach (var part in msg.Parts)
			WritePart(part);

		writer.WriteEndElement();
	}

	private void WritePart(Part part)
	{
		writer.WriteStartElement("part");
		if (part.Name != null)
		{
			writer.WriteAttributeString("name", part.Name.Name);
		}
		if (part.Element != null)
		{
			writer.WriteAttributeString("element", wsdl.PrefixName(part.Element));
		}
		writer.WriteEndElement();
	}

	private void WriteTypes()
	{
		writer.WriteStartElement("types");
		writer.WriteStartElement("schema", "http://www.w3.org/2001/XMLSchema");
		writer.WriteAttributeString("targetNamespace", wsdl.SchemaTargetNamespace);
		writer.WriteAttributeString("elementFormDefault", "qualified");
		writer.WriteAttributeString("attributeFormDefault", "unqualified");
		writer.WriteAttributeString("xmlns", "http://www.w3.org/2001/XMLSchema");

		writer.WriteStartElement("import");
		writer.WriteAttributeString("namespace", "http://schemas.xmlsoap.org/soap/encoding/");
		writer.WriteEndElement();

		var typeVisitor = new TypeXmlWriterVisitor(writer, wsdl);
		foreach (var type in wsdl.Types.Types(wsdl.TargetNamespace))
			type.accept(typeVisitor);

		foreach (var elem in wsdl.Elements)
			WriteElement(elem);

		writer.WriteEndElement();
		writer.WriteEndElement();
	}

	private void WriteElement(Element elem)
	{
		writer.WriteStartElement("element");
		if (elem.Name != null)
		{
			writer.WriteAttributeString("name", elem.Name.Name);
		}
		if (elem.Type != null)
		{
			writer.WriteAttributeString("type", wsdl.PrefixName(elem.Type));
		}
		if (elem.Ref != null)
		{
			writer.WriteAttributeString("ref", wsdl.PrefixName(elem.Ref));
		}
		if (elem.AnonymousType != null)
		{
			var typeVisitor = new TypeXmlWriterVisitor(writer, wsdl);
			elem.AnonymousType.accept(typeVisitor);
		}

		writer.WriteEndElement();
	}
}
