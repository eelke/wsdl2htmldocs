﻿namespace wsdl2htmldocs.naming;

public enum CaseStyle
{
    None,
    Camel,
    Pascal,
    Snake,
    Kebab
}
/// <summary>
/// Class that can convert to and from different naming conventions like
/// CamelCase and snake_case
/// </summary>
public class NormalizedName
{
    public string Original { get; private set; }
    public List<string> Parts { get; private set; }

    public NormalizedName()
    {
        Original = string.Empty;
        Parts = new List<string>();
    }
    public NormalizedName(string s)
    {
        var cs = DetectCaseStyle(s);
        Parts = NormalizeName(s, cs);
        Original = s;
    }

    public bool StartsWith(string s, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
    {
        return StartsWith(new NormalizedName(s), stringComparison);
    }

    public bool StartsWith(NormalizedName start, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
    {
        if (start.Parts.Count <= Parts.Count)
        {
            for (int i = 0; i < start.Parts.Count; ++i)
            {
                if (!start.Parts[i].Equals(Parts[i], stringComparison))
                    return false;
            }
            return true;
        }
        // start is longer 
        return false;
    }

    public NormalizedName SubParts(int startIndex)
    {
        // check is needed for the upper bound because the loop silently returns nothing
        // the lower check is for completeness thought the index operator in de loop would
        // throw anyway
        if (startIndex < 0 || startIndex >= Parts.Count)
            throw new ArgumentOutOfRangeException("startIndex");

        var r = new NormalizedName();
        for (int i = startIndex; i < Parts.Count; ++i)
            r.Parts.Add(Parts[i]);
        return r;
    }


    private static List<string> NormalizeName(string s, CaseStyle cs)
    {
        List<string> r;
        switch (cs)
        {
            case CaseStyle.None:
                r = new List<string>
                {
                    s
                };
                break;
            case CaseStyle.Camel:
            case CaseStyle.Pascal:
                r = SplitOnCase(s);
                break;
            case CaseStyle.Snake:
                r = s.Split('_').ToList<string>();
                break;
            case CaseStyle.Kebab:
                r = s.Split('-').ToList<string>();
                break;
            default:
                throw new ApplicationException("Unexpected value for cs");
        }
        return r;
    }

    private static List<string> SplitOnCase(string s)
    {
        var r = new List<string>();
        if (s.Length <= 1)
        {
            r.Add(s);
        }
        else
        {
            int start = 0;
            for (int i = 1; i < s.Length; ++i)
            {
                if (Char.IsUpper(s, i))
                {
                    r.Add(s.Substring(start, i - start));
                    start = i;
                }
            }
            r.Add(s.Substring(start));
        }
        return r;
    }

    public static CaseStyle DetectCaseStyle(string s)
    {
        if (string.IsNullOrWhiteSpace(s))
            return CaseStyle.None;

        s = s.Trim();

        int uppers = 0;
        int lowers = 0;
        int kebabs = 0;
        int snakes = 0;

        foreach (var c in s)
        {
            if (c == '-') ++kebabs;
            else if (c == '_') ++snakes;
            else if (Char.IsUpper(c)) ++uppers;
            else if (Char.IsLower(c)) ++lowers;
        }
        if (kebabs > 0 && snakes == 0)
            return CaseStyle.Kebab;
        if (snakes > 0 && kebabs == 0)
            return CaseStyle.Snake;
        if (kebabs == 0 && snakes == 0)
        {
            if (uppers > 0 && lowers > 0)
            {
                if (Char.IsUpper(s, 0))
                    return CaseStyle.Pascal;
                return CaseStyle.Camel;
            }
        }
        return CaseStyle.None;
    }

    public string ToSnakeCase()
    {
        string r;
        if (Parts.Count > 0)
        {
            r = Parts[0].ToLowerInvariant();
            for (int i = 1; i < Parts.Count; ++i)
            {
                r += "_";
                r += Parts[i].ToLowerInvariant();
            }
        }
        else
        {
            r = string.Empty;
        }
        return r;
    }

}
