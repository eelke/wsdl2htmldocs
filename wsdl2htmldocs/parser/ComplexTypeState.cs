﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class ComplexTypeState : ParserStateBase
{
    private string _targetNamespace;
    public ComplexType Type { get; private set; }

    public ComplexTypeState(string targetNamespace, XmlReader reader)
    {
        _targetNamespace = targetNamespace;
        var name = Helper.CreateXmlQualifiedName(reader, _targetNamespace, reader.GetAttribute("name"));
        Type = new ComplexType(name);
    }

    override public IParserState? parseElement(XmlReader reader)
    {
        if (reader.NamespaceURI == namespaceXsdURI)
        {
            if (reader.LocalName == "documentation")
            {
                var s = new DocumentationState();
                s.Finished += ds => Type.Documentation = s.Documentation;
                return s;
            }
            else if (reader.LocalName == "element")
            {
                var s = new ElementState(_targetNamespace, reader);
                s.Finished += ds => Type.Elements.Add(s.Element);
                return s;
            }
            else if (reader.LocalName == "extension")
            {
                Type.BaseTypeName = Helper.CreateXmlQualifiedName(reader, _targetNamespace, reader.GetAttribute("base"));
            }
        }
        return null;
    }

}
