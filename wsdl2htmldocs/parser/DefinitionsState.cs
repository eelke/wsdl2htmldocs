﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class DefinitionsState : ParserStateBase
{
    private Wsdl _wsdl = new Wsdl();
    public Wsdl Wsdl { get => _wsdl; }

    public DefinitionsState(XmlReader reader)
    {
        Wsdl.Name = reader.GetAttribute("name") ?? ""; // optional per spec
        Wsdl.TargetNamespace = reader.GetAttribute("targetNamespace") ?? ""; // optional per spec, MUST NOT be relative
        ReadNSAliasses(reader);
    }

    private void ReadNSAliasses(XmlReader reader)
    {
        if (reader.MoveToFirstAttribute())
        {
            do
            {
                if (reader.Prefix == @"xmlns")
                {
                    Wsdl.NamespaceAliasses.Add(reader.LocalName, reader.Value);
                }
            }
            while (reader.MoveToNextAttribute());
        }
    }

    override public IParserState? parseElement(XmlReader reader)
    {
        if (reader.NamespaceURI == namespaceWsdlURI)
        {
            if (reader.LocalName == "portType")
            {
                return new PortTypeState(_wsdl, reader);
            }
            else if (reader.LocalName == "message")
            {
                return new MessageState(_wsdl, reader);
            }
            else if (reader.LocalName == "service")
            {
                var s = new ServiceState(reader);
                s.Finished += st => _wsdl.Service.Merge(s.Service);
                return s;
            }
            else if (reader.LocalName == "binding")
            {
                _wsdl.Binding.Name = reader.GetAttribute("name") ?? string.Empty;
                _wsdl.Binding.Type = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("type"));
            }
        }
        else if (reader.NamespaceURI == namespaceXsdURI)
        {
            if (reader.LocalName == "schema")
            {
                return new SchemaState(_wsdl, reader);
            }
        }
        return null;
    }

}
