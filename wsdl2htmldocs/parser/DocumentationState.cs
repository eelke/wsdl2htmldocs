﻿using System.Xml;

namespace wsdl2htmldocs.parser;

class DocumentationState : ParserStateBase
{
    public string Documentation { get; private set; } = string.Empty;

    override public IParserState? parseElement(XmlReader reader)
    {
        return null;
    }

    override public void parseText(XmlReader reader)
    {
        Documentation = reader.Value;
    }
}
