﻿using System.Xml;
using System.Xml.Linq;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class ElementState : ParserStateBase
{
    private string _targetNamespace;
    public Element Element { get; private set; }

    public ElementState(string targetNamespace, XmlReader reader)
    {
        _targetNamespace = targetNamespace;

        var min = reader.GetAttribute("minOccurs");
        var max = reader.GetAttribute("maxOccurs");
        var minOccurs = min != null ? int.Parse(min) : 1;
        int maxOccurs = 1;
        if (max != null)
        {
            if (max == "unbounded")
            {
                maxOccurs = -1;
            }
            else
            {
                maxOccurs = int.Parse(max);
            }
        }
        XmlQualifiedName name = Helper.CreateXmlQualifiedName(reader, targetNamespace, reader.GetAttribute("name"))
            ?? throw new Exception("Expected element to have a name");

        Element = new Element(name)
        {
            Type = Helper.CreateXmlQualifiedName(reader, targetNamespace, reader.GetAttribute("type")),
            Ref = Helper.CreateXmlQualifiedName(reader, targetNamespace, reader.GetAttribute("ref")),
            MinOccurs = minOccurs,
            MaxOccurs = maxOccurs
        };
    }

    override public IParserState? parseElement(XmlReader reader)
    {
        if (reader.NamespaceURI == namespaceXsdURI)
        {
            if (reader.LocalName == "complexType")
            {
                var s = new ComplexTypeState(_targetNamespace, reader);
                s.Finished += ds => Element.AnonymousType = s.Type;
                s.Type.Owner = Element;
                return s;
            }
            else if (reader.LocalName == "documentation")
            {
                var s = new DocumentationState();
                s.Finished += ds => Element.Documentation = s.Documentation;
                return s;
            }
        }
        return null;
    }
}
