﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class EnumerationState : ParserStateBase
{
    public Enumeration Enumeration { get; private set; }

    public EnumerationState(XmlReader reader)
    {
        var val = reader.GetAttribute("value");
        if (val == null)
            throw new ApplicationException("Enumeration MUST have a value");
        Enumeration = new Enumeration()
        {
            Value = val
        };
    }

    public override IParserState? parseElement(XmlReader reader)
    {
        if (reader.LocalName == "documentation")
        {
            var s = new DocumentationState();
            s.Finished += st => Enumeration.Documentation = s.Documentation;
            return s;
        }
        return null;
    }
}
