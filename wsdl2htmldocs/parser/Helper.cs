﻿using System.Diagnostics.CodeAnalysis;
using System.Xml;

namespace wsdl2htmldocs.parser;

static class Helper
{
	[return: NotNullIfNotNull(nameof(name))]
	static public XmlQualifiedName? CreateXmlQualifiedName(XmlReader reader, string target_namespace, string? name)
	{
		if (name == null)
			return null;

		var colonPosition = name.IndexOf(':');
		if (colonPosition == -1)
		{
			return new XmlQualifiedName(name, target_namespace);
		}

		return new XmlQualifiedName(
			name.Substring(colonPosition + 1),
			reader.LookupNamespace(name.Substring(0, colonPosition))
			);
	}
}
