﻿using System.Xml;

namespace wsdl2htmldocs.parser;

interface IParserState
{
    /// <summary>
    /// Called on start of an element
    /// </summary>
    /// <param name="reader"></param>
    /// <returns>null or e new state, when a new state this state will be removed when the endtag is encountered</returns>
    IParserState? parseElement(XmlReader reader);
    void parseText(XmlReader reader) { }
    /// <summary>
    /// Called when this state is finished
    /// </summary>
    void finished() { }
}
