﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

internal class InitialState : ParserStateBase
{
    private DefinitionsState? tls = null;

    public override IParserState? parseElement(XmlReader reader)
    {
        if (reader.NamespaceURI == namespaceWsdlURI)
        {
            if (reader.LocalName == "definitions")
            {
                return tls = new DefinitionsState(reader);
            }
        }
        return null;
    }

    public Wsdl Wsdl
    {
        get
        {
            if (tls == null)
                throw new InvalidOperationException("Invalid WSDL");

            return tls.Wsdl;
        }
    }

}