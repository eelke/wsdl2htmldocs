﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class MessageState : IParserState
{
	private Wsdl _wsdl;
	private Message _msg;

	public MessageState(Wsdl wsdl, XmlReader reader)
	{
		_wsdl = wsdl;

		string? name = reader.GetAttribute("name");
		if (string.IsNullOrEmpty(name))
		{
			throw new InvalidOperationException("message has no name");
		}
		_msg = new Message(Helper.CreateXmlQualifiedName(reader, wsdl.TargetNamespace, name));
		_wsdl.Messages.Add(_msg.Name, _msg);
	}

	public IParserState? parseElement(XmlReader reader)
	{
		if (reader.LocalName == "part")
		{
			var part = new Part()
			{
				Name = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("name")),
				Element = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("element")),
				Type = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("type"))
			};
			_msg.Parts.Add(part);
		}
		return null;
	}

	public void parseText(XmlReader reader)
	{
	}
}
