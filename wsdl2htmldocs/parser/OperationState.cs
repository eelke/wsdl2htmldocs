﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class OperationState : IParserState
{
    private Wsdl _wsdl;
    private Operation _op;
    enum Expecting
    {
        Nothing,
        Documentation
    };
    Expecting _expecting = Expecting.Nothing;

    public OperationState(Wsdl wsdl, XmlReader reader)
    {
        _wsdl = wsdl;
        // should we report an error is no name is set???
        _op = new Operation(Helper.CreateXmlQualifiedName(
            reader, _wsdl.TargetNamespace, reader.GetAttribute("name") ?? ""));
        _wsdl.PortType.Operations.Add(_op);
    }

    public IParserState? parseElement(XmlReader reader)
    {
        if (reader.Name == "documentation")
        {
            //_op.Documentation = reader.ReadContentAsString();
            _expecting = Expecting.Documentation;
        }
        else if (reader.Name == "input")
        {
            _op.Input = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("message"));
        }
        else if (reader.Name == "output")
        {
            _op.Output = Helper.CreateXmlQualifiedName(reader, _wsdl.TargetNamespace, reader.GetAttribute("message"));
        }
        return null;
    }

    public void parseText(XmlReader reader)
    {
        if (_expecting == Expecting.Documentation)
        {
            _op.Documentation = reader.Value;
            _expecting = Expecting.Nothing;
        }
    }
}
