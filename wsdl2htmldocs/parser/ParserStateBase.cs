﻿using System.Xml;

namespace wsdl2htmldocs.parser;


delegate void NotifyStateFinished(ParserStateBase state);

abstract class ParserStateBase : IParserState
{
    public const string namespaceWsdlURI = "http://schemas.xmlsoap.org/wsdl/";
    public const string namespaceXsdURI = "http://www.w3.org/2001/XMLSchema";

    public event NotifyStateFinished? Finished;

    void IParserState.finished()
    {
        Finished?.Invoke(this);
    }

    public abstract IParserState? parseElement(XmlReader reader);
    public virtual void parseText(XmlReader reader) { }
}
