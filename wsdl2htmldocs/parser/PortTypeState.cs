﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class PortTypeState : IParserState
{
	private readonly Wsdl wsdl;

	public PortTypeState(Wsdl wsdl, XmlReader reader)
	{
		this.wsdl = wsdl;
		wsdl.PortType.Name = reader.GetAttribute("name") ?? "";
	}

	public IParserState? parseElement(XmlReader reader)
	{
		if (reader.Name == "operation")
		{
			return new OperationState(wsdl, reader);
		}
		return null;
	}

	public void parseText(XmlReader reader)
	{
	}
}
