﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class RestrictionState : ParserStateBase
{
    public Restriction Restriction { get; private set; }

    public RestrictionState(string targetNamespace, XmlReader reader)
    {
        string baseName = reader.GetAttribute("base")
            ?? throw new Exception("Expected restriction to allways have a base"); // this might be an incorrect assumption

        Restriction = new Restriction()
        {
            Base = Helper.CreateXmlQualifiedName(reader, targetNamespace, baseName)
        };
    }

    public override IParserState? parseElement(XmlReader reader)
    {
        if (reader.Name == "enumeration")
        {
            var s = new EnumerationState(reader);
            s.Finished += st => Restriction.Enumerations.Add(s.Enumeration);
            return s;
        }
        return null;
    }
}
