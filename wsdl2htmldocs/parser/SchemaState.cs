﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class SchemaState : ParserStateBase
{
	private Wsdl _wsdl;
	private string _targetNamespace;

	public SchemaState(Wsdl wsdl, XmlReader reader)
	{
		_wsdl = wsdl;
		_targetNamespace = reader.GetAttribute("targetNamespace") ?? throw new InvalidOperationException("Expected targetNamespace in schema");
		_wsdl.SchemaTargetNamespace = _targetNamespace;
	}

	override public IParserState? parseElement(XmlReader reader)
	{
		if (reader.NamespaceURI == namespaceXsdURI)
		{
			if (reader.LocalName == "simpleType")
			{
				var s = new SimpleTypeState(_targetNamespace, reader);
				s.Finished += st => _wsdl.Types.Add(s.Type);
				return s;
			}
			else if (reader.LocalName == "complexType")
			{
				var s = new ComplexTypeState(_targetNamespace, reader);
				s.Finished += st => _wsdl.Types.Add(s.Type);
				return s;
			}
			else if (reader.LocalName == "element")
			{
				var s = new ElementState(_targetNamespace, reader);
				s.Finished += st => _wsdl.Elements.Add(s.Element);
				return s;
			}
		}
		return null;
	}

}
