﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

internal class ServicePortState : ParserStateBase
{
    private ServicePort port;
    private XmlReader reader;

    public ServicePortState(ServicePort port, XmlReader reader)
    {
        this.port = port;
        this.reader = reader;

        port.Name = reader.GetAttribute("name") ?? string.Empty;
        port.Binding = Helper.CreateXmlQualifiedName(reader, "", reader.GetAttribute("binding"));
    }

    public override IParserState? parseElement(XmlReader reader)
    {
        if (reader.LocalName == "address" && reader.NamespaceURI == "http://schemas.xmlsoap.org/wsdl/soap/")
        {
            port.AddressLocation = reader.GetAttribute("location") ?? string.Empty;
        }
        return null;
    }
}