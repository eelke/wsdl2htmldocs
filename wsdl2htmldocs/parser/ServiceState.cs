﻿#nullable enable
using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class ServiceState : ParserStateBase
{
    public Service Service { get; private set; }

    public ServiceState(XmlReader reader)
    {
        Service = new Service()
        {
            Name = reader.GetAttribute("name") ?? string.Empty
        };
    }

    override public IParserState? parseElement(XmlReader reader)
    {
        if (reader.Name == "documentation")
        {
            var s = new DocumentationState();
            s.Finished += ds => Service.Documentation = s.Documentation;
            return s;
        }
        else if (reader.Name == "port")
        {
            return new ServicePortState(Service.Port, reader);
        }
        return null;
    }
}
