﻿using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

class SimpleTypeState : ParserStateBase
{
    private string _targetNamespace;
    private SimpleType _type;
    public SimpleType Type { get => _type; }

    public SimpleTypeState(string targetNamespace, XmlReader reader)
    {
        _targetNamespace = targetNamespace;
        var name = Helper.CreateXmlQualifiedName(reader, targetNamespace, reader.GetAttribute("name"))
            ?? throw new Exception("Error expected simpleType to have a name");
        _type = new SimpleType(name);
    }

    override public IParserState? parseElement(XmlReader reader)
    {
        if (reader.NamespaceURI == namespaceXsdURI)
        {
            if (reader.LocalName == "documentation")
            {
                var s = new DocumentationState();
                s.Finished += ds => Type.Documentation = s.Documentation;
                return s;
            }
            else if (reader.LocalName == "restriction")
            {
                var s = new RestrictionState(_targetNamespace, reader);
                s.Finished += st => Type.Restriction = s.Restriction;
                return s;
            }
        }
        return null;
    }
}
