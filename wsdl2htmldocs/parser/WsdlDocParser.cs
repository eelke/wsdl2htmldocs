﻿using System.IO;
using System.Xml;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.parser;

/// <summary>
/// Class that parses a wsdl and collects information for generating documentation
/// </summary>
public class WsdlDocParser
{
    class StackItem
    {
        public int Depth { get; set; }
        public IParserState State { get; set; }

        public StackItem(int depth, IParserState state)
        {
            Depth = depth;
            State = state;
        }
    }

    /// <summary>
    /// Tracks at which XML nesting depth we are.
    /// Depth property of the XmlReader has both the root and it's content at Depth 1
    /// </summary>
    private int xmlParsedepth;
    private readonly Stack<StackItem> states = new();
    private InitialState initialState = new();

    private IParserState CurrentState
    {
        get => states.Peek().State;

        set
        {
            states.Push(new StackItem(xmlParsedepth, value));
        }
    }

    public Wsdl Parse(string filename)
    {
        using StreamReader reader = File.OpenText(filename);
        return Parse(reader);
    }

    public Wsdl Parse(Stream stream)
    {
        using StreamReader reader = new(stream);
        return Parse(reader);
    }

    public Wsdl Parse(TextReader textReader)
    {
        xmlParsedepth = 0;
        states.Clear();
        initialState = new InitialState();
        CurrentState = initialState;

        var settings = new XmlReaderSettings()
        {
            IgnoreWhitespace = true,
            DtdProcessing = DtdProcessing.Ignore
        };

        using var reader = XmlReader.Create(textReader, settings);
        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    if (!reader.IsEmptyElement)
                    {
                        // only increase when not empty as otherwise no item will be put on the stack anyway and we receive no end either
                        ++xmlParsedepth;
                    }
                    var new_state = CurrentState.parseElement(reader);
                    if (new_state != null)
                    {
                        if (reader.IsEmptyElement)
                        {
                            new_state.finished();
                        }
                        else
                        {
                            CurrentState = new_state;
                        }
                    }
                    break;
                case XmlNodeType.Text:
                    //Console.WriteLine($"Inner Text: {reader.Value}");
                    CurrentState.parseText(reader);
                    break;
                case XmlNodeType.EndElement:
                    CheckNeedPopStack();
                    --xmlParsedepth;
                    break;
                default:
                    //Console.WriteLine($"Unknown: {reader.NodeType}");
                    break;
            }
        }
        return initialState.Wsdl;
    }

    private void CheckNeedPopStack()
    {
        if (xmlParsedepth == states.Peek().Depth)
        {
            CurrentState.finished();
            if (xmlParsedepth > 0)
            {
                var parent_state = states.Pop();
            }
        }
    }
}
