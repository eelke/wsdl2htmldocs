﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wsdl2htmldocs.phpgenerator.config;

namespace wsdl2htmldocs.phpgenerator;

internal class FrameworkCode
{
    private readonly PhpGeneratorConfiguration configuration;

    public FrameworkCode(PhpGeneratorConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public void GenerateFrameworkCode()
    {
        WriteFrameworkCode("BaseSoapParser.php");
        WriteFrameworkCode("BaseSoapGenerator.php");
        WriteFrameworkCode("BaseSoapClient.php");
        WriteFrameworkCode("SoapException.php");
    }

    private static string GetFrameworkSourceDir()
    {
        var sourceDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        sourceDir += Path.DirectorySeparatorChar + "phpgenerator" + Path.DirectorySeparatorChar + "php";
        return sourceDir;
    }

    private void WriteFrameworkCode(string filename)
    {
        var fi = new FileInfo(configuration.GetFrameworkPath() + Path.DirectorySeparatorChar + filename);
        using var output = fi.Create();
        using var writer = new StreamWriter(output);

        writer.WriteLine(@"<?php declare(strict_types=1);");
        string fw_ns = configuration.GetFrameworkNamespace();
        if (fw_ns != "")
        {
            writer.WriteLine($@"namespace {fw_ns};");
        }

        var src_fi = new FileInfo(GetFrameworkSourceDir() + Path.DirectorySeparatorChar + filename);
        if (src_fi.Exists)
        {
            using var reader = src_fi.OpenText();
            string s = reader.ReadToEnd();
            writer.Write(s);
        }
        else
        {
            throw new ApplicationException("Framework file is missing " + filename);
        }
    }
}
