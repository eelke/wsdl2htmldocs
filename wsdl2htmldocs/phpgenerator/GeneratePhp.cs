using System.IO;
using System.Xml;
using wsdl2htmldocs.naming;
using wsdl2htmldocs.phpgenerator.config;
using wsdl2htmldocs.phpgenerator.ir;
using wsdl2htmldocs.sidecar;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.phpgenerator;

/// <summary>
/// Generates code for creating a PHP soap client.
/// Note the generated code is not complete. It only contains code that depends on the types
/// in the wsdl.
/// </summary>
/// <remarks>
/// Note the implementation does not support the use of multiple parts. Also note that it is kind of assumed that the whole service definition
/// uses a single namespace.
/// </remarks>
/// 
class GeneratePhp
{
    private readonly Wsdl wsdlData;
    /// <summary>
    /// The first step is to collect the types from the wsdl do some processing on those
    /// and generate ast.Class definitions for them to seperate the processing from the generating
    /// of the PHP code.
    /// </summary>
    private readonly ClassList classes = new();
    private readonly List<IrEnum> enums = new();

    private class BuildinTypeDefinition
    {
        public string PhpTypeName { get; set; }
        public ir.Type Type { get; set; }

        public BuildinTypeDefinition(string phpTypeName, ir.Type t)
        {
            PhpTypeName = phpTypeName;
            Type = t;
        }
    }

    private readonly PhpGeneratorConfiguration configuration;
    private static readonly IReadOnlyDictionary<string, BuildinTypeDefinition> _buildinTypes = CreateBuildinTypes();

    public GeneratePhp(PhpGeneratorConfiguration configuration, Wsdl wsdl)
    {
        this.configuration = configuration;
        this.wsdlData = wsdl;

        CreateMissingPaths();

    }

    private static Dictionary<string, BuildinTypeDefinition> CreateBuildinTypes()
    {
        var defInt = new BuildinTypeDefinition(@"int", new TypeInt());
        Dictionary<string, BuildinTypeDefinition> result = new()
        {
            { "boolean", new BuildinTypeDefinition(@"bool", new TypeBool()) },
            { "date", new BuildinTypeDefinition(@"\DateTime", new TypeDate()) },
            { "dateTime", new BuildinTypeDefinition(@"\DateTime", new TypeDateTime()) },
            { "decimal", new BuildinTypeDefinition(@"BigDecimal", new TypeDecimal()) },
            { "int", defInt },
            { "long", defInt },
            { "unsignedByte", defInt },
            { "short", defInt },
            { "string", new BuildinTypeDefinition(@"string", new TypeString()) }
        };
        return result;
    }

    public void Generate()
    {
        classes.Clear();
        enums.Clear();

        MakeMappingProposals();
        GenerateIrEnums();
        GenerateIrClasses();

        new FrameworkCode(configuration).GenerateFrameworkCode();
        new PhpCodeWriter(
            configuration,
            classes,
            enums,
            wsdlData,
            GenerateSoapClient()
            ).Generate();

        // Note when adding a file here do not forget to set the "Copy to output directory"
        // property of the source file to "Copy if newer" or the file won't be found
        // at runtime
        Console.Out.WriteLine($"Classes {classes.Count}, enums {enums.Count}");
        Console.Out.WriteLine("PHP client generated!");
    }


    private readonly Dictionary<string, string> classNames = new();


    private void MakeMappingProposals()
    {
        foreach (var ns in wsdlData.Types.Namespaces())
        {
            foreach (var type in wsdlData.Types.Types(ns))
            {
                if (type is ComplexType complex)
                {
                    MakeMappingProposalsForType(complex, type.Name);
                }
            }
        }
        // Their are also complex types that are not listed as available types 
        // because they are anonymous declares as part of an element
        // But we do need serialization for them
        foreach (var elem in wsdlData.Elements)
        {
            if (elem.AnonymousType != null)
            {
                // element has inline declared type
                if (elem.AnonymousType is ComplexType ct)
                {
                    MakeMappingProposalsForType(ct, elem.Name);
                }
            }
        }
    }

    private void MakeMappingProposalsForType(ComplexType ct, XmlQualifiedName name)
    {
        sidecar.Type? t = configuration.Sidecar.PhpConfig.FindType(name.Name);
        bool new_type = false;
        if (t == null)
        {
            t = new sidecar.Type()
            {
                WsdlName = name.Name
            };
            new_type = true;
        }

        foreach (var elem in ct.Elements)
        {
            var mapping = t.FindMappingForWsdlField(elem.Name.Name);
            if (mapping == null)
            {
                var proposal = ProposeSpecialMapping(elem, ct.Elements, name.Name);
                if (proposal != null)
                    t.Mappings.Add(proposal);
            }
        }

        if (new_type && t.Mappings.Count > 0)
        {
            configuration.Sidecar.PhpConfig.Types.Add(t);
        }
    }

    private Mapping? ProposeSpecialMapping(Element elem, List<Element> elements, string wsdlName)
    {
        if (configuration.Params.AutoDecimalMappings)
        {
            Mapping? result = ConsiderDecimalMapping(elem, elements, wsdlName);
            if (result is { })
                return result;
        }

        return null;
    }

    internal static Mapping? ConsiderDecimalMapping(Element elem, List<Element> elements, string wsdlName)
    {
        string[] candidateWords = new string[] { "price", "quantity", "amount" };

        int index = elements.IndexOf(elem);

        if (elem.Type.Namespace == "http://www.w3.org/2001/XMLSchema" && elem.Type.Name == "long")
        {
            foreach (var kw in candidateWords)
            {
                if (elem.Name.Name.Contains(kw, StringComparison.OrdinalIgnoreCase))
                {
                    // is candidate

                    if (index < elements.Count - 1)
                    {
                        var dp = elements[index + 1];
                        if (dp.Type.Namespace == "http://www.w3.org/2001/XMLSchema" && dp.Type.Name == "int")
                        {
                            var dp_name = dp.Name.Name;
                            const string dpstr = "decimalPlaces";
                            bool match = false;
                            if (dp_name == dpstr)
                            {
                                match = true;
                            }
                            else if (dp_name.EndsWith(dpstr, StringComparison.OrdinalIgnoreCase))
                            {
                                if (elem.Name.Name.StartsWith(dp_name.Substring(0, dp_name.Length - dpstr.Length), StringComparison.OrdinalIgnoreCase))
                                    match = true;
                            }
                            else if (dp_name.StartsWith(dpstr, StringComparison.OrdinalIgnoreCase))
                            {
                                var prefix = dp_name.Substring(dpstr.Length);
                                if (elem.Name.Name.EndsWith(prefix, StringComparison.OrdinalIgnoreCase))
                                    match = true;
                            }
                            if (match)
                            {
                                Console.Out.WriteLine($"notice: using automatic BigDecimal mapping for {wsdlName}.{elem.Name.Name} with scale {dp.Name.Name}");
                                return new Mapping()
                                {
                                    Type = MappingType.LongToDecimal,
                                    Field = elem.Name.Name,
                                    Scale = dp_name,
                                    DefaultDecimals = 2
                                };
                            }
                        }
                    }
                    Console.Out.WriteLine($"notice: using automatic BigDecimal mapping for {wsdlName}.{elem.Name.Name}");
                    return new Mapping()
                    {
                        Type = MappingType.LongToDecimal,
                        Field = elem.Name.Name,
                        DefaultDecimals = 2
                    };
                }
            }
        }
        return null;
    }

    private void GenerateIrEnums()
    {
        foreach (var ns in wsdlData.Types.Namespaces())
        {
            foreach (var type in wsdlData.Types.Types(ns))
            {
                if (type is SimpleType simple)
                {
                    enums.Add(simple.ToIrEnum(configuration));
                }
            }
        }
    }

    private void GenerateIrClasses()
    {
        classNames.Clear();

        foreach (var ns in wsdlData.Types.Namespaces())
        {
            foreach (var type in wsdlData.Types.Types(ns))
            {
                if (type is ComplexType complex && type.Name != null)
                {
                    var astcls = GenerateClassForType(complex, type.Name);
                    classes.Add(astcls);
                }
            }
        }
        // Their are also complex types that are not listed as available types 
        // because they are anonymous declares as part of an element
        // But we do need serialization for them
        foreach (var elem in wsdlData.Elements)
        {
            if (elem.AnonymousType != null)
            {
                // element has inline declared type
                if (elem.AnonymousType is ComplexType ct)
                {
                    var astcls = GenerateClassForType(ct, elem.Name);
                    classes.Add(astcls);
                }
            }
        }
    }

    private sidecar.Type? GetSideCarType(string name)
    {
        return configuration.Sidecar.PhpConfig?.FindType(name);
    }

    private Class GenerateClassForType(ComplexType ct, XmlQualifiedName elemName)
    {
        var name = configuration.AsTypeIdent(elemName);
        WarnForDuplicates(classNames, name);

        sidecar.Type? t = GetSideCarType(elemName.Name);
        if (t == null)
        {
            // so GenerateClassProperties can assume it is always valid
            t = new sidecar.Type();
        }

        var cls = new Class(name, elemName.Name);
        if (ct.BaseTypeName != null)
        {
            cls.Extends = configuration.AsTypeIdent(ct.BaseTypeName);
        }

        if (!string.IsNullOrEmpty(t.ImplementationFile))
        {
            if (File.Exists(t.ImplementationFile))
            {
                cls.ImplementationFile = t.ImplementationFile;
            }
            else
            {
                Console.Error.WriteLine($"Error: implementation file {t.ImplementationFile} missing");
            }
        }

        GenerateClassProperties(t, ct.Elements, ref cls);
        return cls;
    }

    private void GenerateClassProperties(sidecar.Type t, List<Element> elements, ref Class cls)
    {
        foreach (var elem in elements)
        {
            GenerateProperty(t, cls, elem);
        }
    }

    private void GenerateProperty(sidecar.Type t, Class cls, Element elem)
    {
        var mapping = t.FindMappingForWsdlField(elem.Name.Name) ?? Mapping.DirectMappingInstance;
        bool optional = false;

        var conversion_cfg = GetSideCarType(elem.Type.Name)?.Conversion;
        ir.Type type = GetAsIrType(elem.Type);
        ir.Type? target_type = null;
        if (conversion_cfg != null)
        {
            target_type = new TypeClass()
            {
                ClassName = conversion_cfg.TargetType
            };
        }

        bool listWrapper = false;
        string? elemIdent = null;
        if (conversion_cfg == null && elem.MaxOccurs == 1 && configuration.Params.RemoveListWrappers)
        {
            if (wsdlData.Types.Find(elem.Type) is ComplexType complexType)
            {
                if (configuration.IsListWrapper(complexType))
                {
                    listWrapper = true;
                    elemIdent = PhpUtil.AsIdent(complexType.Elements[0].Name!);
                    // declaration should be of type on element type
                    type = new TypeArray(GetAsIrType(complexType.Elements[0].Type!));
                }
            }
        }

        // assumption max = 1 or larger or unbounded, unbounded is represented by -1
        // so we test unequal
        if (elem.MaxOccurs != 1)
        {
            // this is a list, 
            var at = new TypeArray(type);

            type = at;
        }
        else if (elem.MinOccurs == 0)
        {
            optional = true;
        }

        var propName = PhpUtil.AsIdent(elem.Name);
        var prop = new Property()
        {
            Name = propName,
            ElemName = elem.Name.Name,
            Optional = optional,
            Type = target_type ?? type
        };

        Property? declareProp = null;
        PropertyReader? readMapping = null;
        PropertyWriter? writeMapping = null;

        switch (mapping.Type)
        {
            case MappingType.Direct:
                declareProp = prop;
                if (listWrapper)
                {
                    readMapping = new ListWrapperPropertyReader(prop, GetPhpType(elem.Type), elemIdent);
                    writeMapping = new ListWrapperPropertyWriter(prop, GetPhpType(elem.Type));
                }
                else if (conversion_cfg == null)
                {
                    readMapping = new DefaultPropertyReader(prop);
                    writeMapping = new DefaultPropertyWriter(prop);
                }
                else
                {
                    if (type is TypeClass classType)
                    {
                        {
                            string target = prop.DeserializeLocal ? $"${prop.Name}" : $"$o->{prop.Name}";
                            var deserializeObject = $"$this->load_{classType.ClassName}($in)";
                            var conversionExp = string.Format(conversion_cfg.ConvertToExpressionFmt, deserializeObject);
                            var readStatement = $"{target} = {conversionExp}";
                            readMapping = new CustomExpressionPropertyReader(prop, readStatement);
                        }
                        {
                            var conversionExp = string.Format(conversion_cfg.ConvertFromExpressionFmt, $"$this->{propName}");
                            var exp = $"({conversionExp})->write($gen, '{propName}');";
                            writeMapping = new CustomExpressionPropertyWriter(prop, exp);
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("Error: conversions should only be defined on ComplexTypes");
                    }
                }
                break;

            case MappingType.LongToDecimal:
                // both fields should be deserialized locally because we are going to pick
                // them up in a postprocessing step.
                prop.DeserializeLocal = true;
                readMapping = new DefaultPropertyReader(prop);

                // We only need one declaration and serializer so we do that on the value field
                if (elem.Name.Name == mapping.Field)
                {
                    declareProp = new Property()
                    {
                        Name = propName,
                        ElemName = mapping.Field,
                        Optional = optional,
                        Type = new TypeDecimal()
                    };
                    writeMapping = new DecimalToValueScalePropertyWriter()
                    {
                        ObjectField = declareProp,
                        ValueField = mapping.Field,
                        ScaleField = mapping.Scale,
                        DefaultScale = mapping.DefaultDecimals
                    };

                    var scale = string.IsNullOrEmpty(mapping.Scale) ? $"{mapping.DefaultDecimals}"
                        : ("$" + PhpUtil.AsIdent(mapping.Scale) + $" ?? {mapping.DefaultDecimals}");
                    cls.PostDeserializeActions.Add(new PostDeserializeAction()
                    {
                        CodeFmt = $"{{0}}->{propName} = isset(${propName}) ? BigDecimal::ofUnscaledValue(${propName}, {scale}) : null;"
                    });
                }
                break;

            default:
                throw new NotImplementedException("Unsupported MappingType");
        }

        if (declareProp != null)
            cls.DeclareProperties.Add(declareProp);
        if (readMapping != null)
            cls.ReadProperties.Add(readMapping);
        if (writeMapping != null)
            cls.WriteProperties.Add(writeMapping);
    }

    private ClientClass GenerateSoapClient()
    {
        string fw_ns = configuration.GetFrameworkNamespace(true);
        string clsname = configuration.GetSoapClientClassName();

        ClientClass client = new()
        {
            ClassName = clsname
        };

        foreach (var op in wsdlData.PortType.Operations)
        {
            client.Operations.Add(
                CreateClientOperation(op)
                );
        }
        return client;
    }

    private ClientOperation CreateClientOperation(Operation op)
    {
        return new ClientOperation()
        {
            OperationName = op.Name.Name,
            RequestClass = GetRequestClass(op),
            ResultTypeName = GetOutputTypeName(op) // limitation we only expect one return type here, soap does support multiple
        };
    }

    private string GetOutputTypeName(Operation op)
    {
        soap.Type? output_type = GetTypeForMessage(op.Output);
        if (output_type == null)
        {
            return "";
        }

        if (output_type.Name != null)
        {
            return GetPhpType(output_type.Name);
        }

        // afdalen in type hopelijk heeft hij maar 1 element
        if (output_type is ComplexType outputComplex)
        {
            if (outputComplex.Owner != null && outputComplex.Owner.Name != null)
            {
                return configuration.AsTypeIdent(outputComplex.Owner.Name);
            }
            else
            {
                // Expected that either output_type had a name or that it is a complexType with an owner
                throw new InvalidOperationException("Unexpected null value while processing output type");
            }
        }
        throw new InvalidOperationException("Unexpected construct used in output type of operation");
    }

    private Class? GetRequestClass(Operation op)
    {
        if (op.Input != null)
        {
            Part input_part = GetSinglePartForMessage(op.Input);
            soap.Type input_type = GetTypeForPart(input_part);
            var elem_name = configuration.AsTypeIdent(input_part.Element!);
            return classes.GetByPhpName(elem_name);
        }
        return null;
    }

    private soap.Type? GetTypeForMessage(XmlQualifiedName? msgName)
    {
        if (msgName == null)
            return null;

        if (wsdlData.Messages.TryGetValue(msgName, out Message? msg))
        {
            if (msg.Parts.Count == 1)
                return GetTypeForPart(msg.Parts[0]);
            else if (msg.Parts.Count > 1)
                throw new ApplicationException("Multiple parts are not supported");
            else
                return null; // zero parts
        }
        throw new ApplicationException($"Could not resolve message {msgName}");
    }

    private Part GetSinglePartForMessage(XmlQualifiedName msgName)
    {
        if (wsdlData.Messages.TryGetValue(msgName, out Message? msg))
        {
            if (msg.Parts.Count == 1)
                return msg.Parts[0];
            else if (msg.Parts.Count > 1)
                throw new ApplicationException("Single part expected in GetSinglePartForMessage");
        }
        throw new ApplicationException($"Could not resolve message {msgName}");
    }

    private soap.Type GetTypeForPart(Part p)
    {
        if (p.Element != null)
        {
            var r = wsdlData.Elements.Find(e => e.Name == p.Element);
            if (r != null)
            {
                if (r.AnonymousType != null)
                {
                    return r.AnonymousType;
                }
                else if (r.Type != null)
                {
                    var t = wsdlData.Types.Find(r.Type.Namespace, r.Type.Name);
                    if (t != null)
                    {
                        return t;
                    }
                }
            }
        }
        if (p.Type != null)
        {
            var t = wsdlData.Types.Find(p.Type.Namespace, p.Type.Name);
            if (t != null)
            {
                return t;
            }
        }
        throw new ApplicationException($"Cannot find type for part with name {p.Name} and element {p.Element}");
    }

    private void CreateMissingPaths()
    {
        var of = configuration.GetOutputFile("Dummy.php");
        if (of.Directory != null)
            Directory.CreateDirectory(of.Directory.FullName);
        Directory.CreateDirectory(configuration.GetFrameworkPath());
    }

    static private bool WarnForDuplicates(Dictionary<string, string> classNames, string name)
    {
        var lcname = name.ToLower();
        if (classNames.TryGetValue(lcname, out var conflicting))
        {
            Console.WriteLine($"WARNING: name {name} conflicts with {conflicting}");
            return false;
        }
        else
        {
            classNames.Add(lcname, name);
            return true;
        }
    }

    private ir.Type GetAsIrType(XmlQualifiedName qname)
    {
        if (qname.Namespace == "http://schemas.xmlsoap.org/soap/encoding/")
        {
            switch (qname.Name)
            {
                case "base64":
                default:
                    return new TypeString();
            }
        }
        else if (qname.Namespace == "http://www.w3.org/2001/XMLSchema")
        {
            if (_buildinTypes.TryGetValue(qname.Name, out var def))
            {
                return def.Type;
            }
        }
        else
        {
            var elem_type = wsdlData.Types.Find(qname);
            if (elem_type != null)
            {
                if (elem_type is SimpleType simple)
                {
                    // simple types are allways enums
                    return new TypeString();
                }
            }
        }
        return new TypeClass()
        {
            ClassName = configuration.AsTypeIdent(qname)
        };
    }

    private string GetPhpType(XmlQualifiedName qname)
    {
        if (qname.Namespace == "http://schemas.xmlsoap.org/soap/encoding/")
        {
            switch (qname.Name)
            {
                case "base64":
                default:
                    return "string";
            }
        }
        else if (qname.Namespace == "http://www.w3.org/2001/XMLSchema")
        {
            if (_buildinTypes.TryGetValue(qname.Name, out var def))
            {
                return def.PhpTypeName;
            }
        }
        else
        {
            var elem_type = wsdlData.Types.Find(qname);
            if (elem_type != null)
            {
                if (elem_type is SimpleType simple)
                {
                    // simple types are allways enums
                    return "string";
                }
            }
        }
        return configuration.AsTypeIdent(qname);
    }
}
