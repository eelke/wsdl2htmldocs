﻿using System.IO;
using wsdl2htmldocs.sidecar;

namespace wsdl2htmldocs.phpgenerator;

public class GeneratePhpHandler : IGeneratePhpHandler
{
    public void Handle(GeneratePhpParams config)
    {
        WsdlSidecar? wsdlSidecar = null;

        var sidecar_filename = config.Wsdls[0].FullName + ".w2dsc";
        if (File.Exists(sidecar_filename))
        {
            wsdlSidecar = WsdlSidecar.Load(sidecar_filename);
        }
        var wsdl = CommandHandlerUtil.LoadWsdls(config.Wsdls);
        var generator = new GeneratePhp(
            new(config, wsdlSidecar ?? new WsdlSidecar()),
            wsdl
        );
        generator.Generate();

        Console.Out.WriteLine("Finished");
    }
}
