﻿using System.IO;

namespace wsdl2htmldocs.phpgenerator;

public class GeneratePhpParams
{
    /// <summary>
    /// The name and optional a path for the output file.
    /// This is stored in a string as the generator needs to know if the path was specified or not this
    /// information is lost when using FileInfo.
    /// </summary>
    public string? Output { get; set; }
    public DirectoryInfo? OutputBasepath { get; set; }
    public List<FileInfo> Wsdls { get; set; } = new();
    public bool AutoDecimalMappings { get; set; }
    public bool RemoveListWrappers { get; set; }
}
