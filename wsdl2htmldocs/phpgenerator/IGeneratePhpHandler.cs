﻿namespace wsdl2htmldocs.phpgenerator;

public interface IGeneratePhpHandler
{
    void Handle(GeneratePhpParams config);
}
