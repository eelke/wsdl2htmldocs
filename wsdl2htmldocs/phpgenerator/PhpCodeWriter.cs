﻿using System.IO;
using System.IO.Abstractions;
using System.Text;
using wsdl2htmldocs.phpgenerator.config;
using wsdl2htmldocs.phpgenerator.ir;
using wsdl2htmldocs.phpgenerator.phpsourcegenerator;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.phpgenerator;

internal class PhpCodeWriter
{
    private readonly PhpGeneratorConfiguration configuration;
    private readonly ClassList classes;
    private readonly IReadOnlyList<ir.IrEnum> enums;
    private readonly ClientClass clientClass;
    private readonly Wsdl wsdlData;

    public PhpCodeWriter(PhpGeneratorConfiguration configuration, ClassList classes, IReadOnlyList<ir.IrEnum> enums, Wsdl wsdlData, ClientClass clientClass)
    {
        this.configuration = configuration;
        this.classes = classes;
        this.enums = enums;
        this.wsdlData = wsdlData;
        this.clientClass = clientClass;
    }

    public void Generate()
    {
        string clientNamespaceName = configuration.GetClientNamespace(wsdlData);

        WriteClientFile(configuration.GetOutputFile(configuration.GetSoapClientFileName()), WriteSoapClient);

        using (var writer = CreateStreamWriter("Enums.php"))
        {
            GeneratePhpEnums(new FileBuilder(writer, clientNamespaceName));
        }

        WriteClientFile(configuration.GetOutputFile("SoapObjects.php"), (StreamWriter writer) =>
        {
            writer.WriteLine(@"use Brick\Math\BigDecimal;");
            GenerateClasses(new Writer(writer));
        });
        WriteClientFile(configuration.GetOutputFile("SoapParser.php"), (StreamWriter writer) =>
        {
            string fw_ns = configuration.GetFrameworkNamespace(true);
            writer.WriteLine($@"use {fw_ns}BaseSoapParser;");
            writer.WriteLine(@"use Brick\Math\BigDecimal;");
            GenerateParser(new Writer(writer));
        });
        WriteClientFile(configuration.GetOutputFile("SoapGenerator.php"), (StreamWriter writer) =>
        {
            string fw_ns = configuration.GetFrameworkNamespace(true);
            writer.WriteLine($@"use {fw_ns}BaseSoapGenerator;");
            writer.WriteLine($@"use {fw_ns}SoapSerializationException;");
            var w = new Writer(writer);
            GenerateGenerator(w);
        });

        // Note when adding a file here do not forget to set the "Copy to output directory"
        // property of the source file to "Copy if newer" or the file won't be found
        // at runtime
        Console.Out.WriteLine($"Classes {classes.Count}, enums {enums.Count}");
        Console.Out.WriteLine("PHP client generated!");
    }

    private StreamWriter CreateStreamWriter(string filename)
    {
        var fileInfo = configuration.GetOutputFile(filename);
        return new StreamWriter(fileInfo.Create());
    }

    private void WriteClientFile(IFileInfo output_file, Action<StreamWriter> write_action)
    {
        string ns = configuration.GetClientNamespace(wsdlData);

        using var output = output_file.Create();
        using var writer = new StreamWriter(output);

        writer.WriteLine(@"<?php declare(strict_types=1);");
        writer.WriteLine(@"// Generated code");
        writer.WriteLine($"namespace {ns};");

        write_action(writer);
    }

    private void WriteSoapClient(StreamWriter writer)
    {
        string fw_ns = configuration.GetFrameworkNamespace(true);
        string clsname = configuration.GetSoapClientClassName();
        writer.WriteLine($@"use {fw_ns}BaseSoapClient;");
        writer.WriteLine(@"use Brick\Math\BigDecimal;");
        writer.WriteLine($@"class {clsname} extends BaseSoapClient {{");
        writer.WriteLine("""    
            public function __construct(string $apiServer, int $apiPort, string $ident, string $secret, ?float $connectTimeout = null, ?float $timeout = null, bool $verify = true) {
                parent::__construct($apiServer, $apiPort, $ident, $secret, $connectTimeout, $timeout, $verify);
                $this->parser = new SoapParser();
            }
        """);
        foreach (var op in clientClass.Operations)
        {
            WriteOperationFunc(writer, op);
        }

        writer.WriteLine("}"); // end of generator class
    }


    private void WriteOperationFunc(StreamWriter writer, ClientOperation clientOperation)
    {
        try
        {
            StringBuilder inputs = new();
            if (clientOperation.RequestClass != null)
            {
                foreach (var prop in clientOperation.RequestClass.DeclareProperties)
                {
                    inputs.Append(prop.Type.GetTypeHint());
                    inputs.Append(" $");
                    inputs.Append(prop.Name);
                    inputs.Append(", ");
                }
            }
            inputs.Append("?string $requestId = null");

            string returnTypeDeclaration = string.IsNullOrEmpty(clientOperation.ResultTypeName)
                ? ""
                : $": {clientOperation.ResultTypeName} ";

            var functionName = PhpUtil.AsIdent(clientOperation.OperationName);

            writer.WriteLine($"    public function {functionName}({inputs}) {returnTypeDeclaration}{{");
            // serialize parameters if multiple we have to put them in the element type first.
            writer.WriteLine($"        $opname = '{clientOperation.OperationName}';");
            writer.WriteLine($"        $this->startRequest($opname);");
            writer.WriteLine($"        $reqobj = new {clientOperation.RequestClass.PhpName}();");
            foreach (var prop in clientOperation.RequestClass.DeclareProperties)
            {
                writer.WriteLine($"        $reqobj->{prop.Name} = ${prop.Name};");
            }
            writer.WriteLine($"        $gen = new SoapGenerator();");
            writer.WriteLine($"        $rq = $gen->write($reqobj, $opname);");
            // send request and receive response
            writer.WriteLine($"        $resp = $this->communicate($opname, $rq, $requestId);");
            // parse result
            writer.WriteLine($"        $res = $this->parser->parse($resp);");
            writer.WriteLine($"        $this->endRequest();");
            writer.WriteLine($"        return $res;");
            writer.WriteLine("    }"); // end of function
        }
        catch (Exception ex)
        {
            throw new ApplicationException($"Error generating operation function for {clientOperation.OperationName}", ex);
        }
    }


    private void GenerateGenerator(Writer writer)
    {
        writer.WriteLine("class SoapGenerator extends BaseSoapGenerator {");
        using (var g1 = writer.IncreaseIdent())
        {
            writer.WriteLine("public function __construct(bool $pretty = false) {");
            writer.WriteLine($"\tparent::__construct('{wsdlData.TargetNamespace}', $pretty);");
            writer.WriteLine("}");
            writer.WriteLine(@"public function write(SoapObject $msg, string $elemName) : string {");
            using (var g2 = writer.IncreaseIdent())
            {
                writer.WriteLine(@"try {");
                using (var g3 = writer.IncreaseIdent())
                {
                    writer.WriteLine(@"$this->writeMessageStart();");
                    writer.WriteLine(@"$msg->write($this, $elemName);");
                    writer.WriteLine(@"$this->writeMessageEnd();");
                    writer.WriteLine(@"$s = $this->out->outputMemory(true); ");
                    writer.WriteLine(@"unset($this->out);");
                    writer.WriteLine(@"return $s; ");
                }
                writer.WriteLine(@"}");
                writer.WriteLine(@"catch (\Exception $e) {");
                writer.WriteLine("\tthrow new SoapSerializationException('Error while generating soap request', 0, $e);");
                writer.WriteLine(@"}");
            }
            writer.WriteLine(@"}");
        }
        writer.WriteLine("}");
    }



    private void GenerateParser(Writer writer)
    {
        writer.WriteLine("class SoapParser extends BaseSoapParser {");
        using (var g1 = writer.IncreaseIdent())
        {
            Generate_loadSoapObjectByName_func(writer);
            Generate_loadClass_funcs(writer);
        }
        writer.WriteLine("}"); // end of parser class
    }

    private void Generate_loadClass_funcs(Writer writer)
    {
        foreach (var cls in classes)
        {
            Generate_loadClass_funcs(writer, cls);
        }
    }

    private void Generate_loadClass_funcs(Writer writer, ir.Class cls)
    {
        writer.WriteLine($"private function load_{cls.PhpName}(\\XMLReader $in) : {cls.PhpName} {{");
        using (var g1 = writer.IncreaseIdent())
        {
            writer.WriteLine("$n = $in->name;");
            writer.WriteLine($"$o = new {cls.PhpName}();");
            writer.WriteLine("if ($in->isEmptyElement) return $o;");
            writer.WriteLine("$continue = true;");
            writer.WriteLine("while ($continue && $in->read()) {");
            using (var g3 = writer.IncreaseIdent())
            {
                writer.WriteLine("switch ($in->nodeType) {");
                using (var g4 = writer.IncreaseIdent())
                {
                    writer.WriteLine("case \\XMLReader::ELEMENT:");
                    using (var g_case1 = writer.IncreaseIdent())
                    {
                        writer.WriteLine("switch ($in->localName) {");
                        using (var g_case = writer.IncreaseIdent())
                        {
                            Generate_loadClass_func_allProperties(writer, cls);
                            //Generate_loadClass_func_properties(writer, cls.ReadProperties);
                        }
                        writer.WriteLine("}");
                        writer.WriteLine("break;");
                    }
                    writer.WriteLine("case \\XMLReader::END_ELEMENT:");
                    using (var g_case2 = writer.IncreaseIdent())
                    {
                        writer.WriteLine("if ($in->name == $n) $continue = false;");
                        writer.WriteLine("break;");
                    }
                }
                writer.WriteLine("}"); // end switch
            }
            writer.WriteLine("}"); // end while
            Generate_loadClass_func_postdeserialize(writer, cls.PostDeserializeActions);
            writer.WriteLine("return $o;");
        }
        writer.WriteLine("}"); // end of function

    }

    private void Generate_loadClass_func_allProperties(Writer writer, ir.Class cls)
    {
        if (!string.IsNullOrEmpty(cls.Extends))
        {
            var baseClass = classes.Where(c => c.PhpName == cls.Extends).ToArray();
            if (baseClass.Length == 0)
            {
                throw new ApplicationException("Type note found " + cls.Extends);
            }
            Generate_loadClass_func_allProperties(writer, baseClass[0]);
        }
        Generate_loadClass_func_properties(writer, cls.ReadProperties);
    }

    private void Generate_loadClass_func_properties(Writer writer, List<ir.PropertyReader> readers)
    {
        foreach (var reader in readers)
        {
            string deserialize_code = reader.GetDeserializeCode();
            writer.WriteLine($"case '{reader.Property.ElemName}': {deserialize_code}; break;");
        }
    }

    private void Generate_loadClass_func_postdeserialize(Writer writer, List<ir.PostDeserializeAction> postDeserializeActions)
    {
        foreach (var pdsa in postDeserializeActions)
        {
            var s = string.Format(pdsa.CodeFmt, "$o");
            writer.WriteLine(s);
        }
    }

    private void Generate_loadSoapObjectByName_func(Writer writer)
    {
        writer.WriteLine("protected function loadSoapObjectByName(\\XMLReader $in) {");
        using (var g1 = writer.IncreaseIdent())
        {
            writer.WriteLine("switch ($in->localName) {");
            using (var g2 = writer.IncreaseIdent())
            {
                foreach (var cls in classes)
                {
                    writer.WriteLine($"case '{cls.WsdlName}': return $this->load_{cls.PhpName}($in);");
                }
            }
            writer.WriteLine("}"); // end of switch
        }
        writer.WriteLine("}"); // end of function
    }

    private void GenerateClasses(Writer writer)
    {
        writer.WriteLine("abstract class SoapObject {");
        using (var g1 = writer.IncreaseIdent())
        {
            writer.WriteLine("const TNS = 'ns1';");
            writer.WriteLine("abstract public function write(SoapGenerator $gen, string $elemName): void;");
            writer.WriteLine("""
                public function __set(string $name, mixed $value): void { 
                    throw new \Exception("$name is not an allowed property of this request");
                }
                """);
        }
        writer.WriteLine("}");
        foreach (var cls in classes)
        {
            GenerateClass(writer, cls);
            writer.WriteLine("");
        }
    }

    private void GenerateClass(Writer writer, Class cls)
    {
        var baseClass = cls.Extends ?? "SoapObject";
        writer.WriteLine($"class {cls.PhpName} extends {baseClass} {{");
        using (var g1 = writer.IncreaseIdent())
        {
            foreach (var prop in cls.DeclareProperties)
            {
                //writer.WriteLine(prop.GetDeclaration());
                writer.WriteIndent();
                prop.WriteDeclaration(writer);
            }

            GenerateClassConstructor(writer, cls);
            GenerateClassWritePropertiesMethod(writer, cls);
            GenerateClassWriteMethod(writer);

            if (cls.ImplementationFile != null)
            {
                string inc = System.IO.File.ReadAllText(cls.ImplementationFile);
                writer.WriteRaw(inc);
            }
        }
        writer.WriteLine("}");
    }

    private void GenerateClassConstructor(Writer writer, Class cls)
    {
        // When listwrapper then generate a constructor that accepts an array
        if (cls.DeclareProperties.Count == 1)
        {
            var prop = cls.DeclareProperties[0];
            var typeArray = prop.Type as ir.TypeArray;
            if (typeArray != null)
            {
                writer.WriteLine($"public function __construct($list = array()) {{ $this->{prop.Name} = $list; }}");
                return;
            }
        }

        bool first = true;

        Writer.IdentGuard? ig = null;
        foreach (var prop in cls.DeclareProperties)
        {
            if (!prop.Optional)
            {
                string i = prop.Type.GetConstructorInitializer();
                if (!string.IsNullOrEmpty(i))
                {
                    if (first == true)
                    {
                        first = false;
                        writer.WriteLine("public function __construct() {");
                        ig = writer.IncreaseIdent();
                    }
                    writer.WriteLine($"$this->{prop.Name} = {i};");
                }
            }
        }
        if (!first)
        {
            if (ig != null) ig.Dispose();
            writer.WriteLine("}");
        }

    }

    private void GenerateClassWritePropertiesMethod(Writer writer, Class cls)
    {
        writer.WriteLine("public function writeProps(SoapGenerator $gen): void {");
        if (cls.Extends != null)
        {
            writer.WriteLine("    parent::writeProps($gen);");
        }
        using (var g2 = writer.IncreaseIdent())
        {
            foreach (var w in cls.WriteProperties)
            {
                string code = w.GetSerializationCode();
                writer.WriteLine(code);
            }
        }
        writer.WriteLine("}");
    }

    private void GenerateClassWriteMethod(Writer writer)
    {
        writer.WriteLine("public function write(SoapGenerator $gen, string $elemName): void {");
        using (var g2 = writer.IncreaseIdent())
        {
            //                    writer.WriteLine($"$gen->write_{cls.PhpName}_toXml($this, $elemName);");
            const string prefix = "self::TNS";
            writer.WriteLine($"$gen->out->startElementNs({prefix}, $elemName, null);");
            writer.WriteLine($"$this->writeProps($gen);");
            writer.WriteLine($"$gen->out->endElement();");

        }
        writer.WriteLine("}");
    }

    public void GeneratePhpEnums(IFileBuilder phpFile)
    {
        foreach (var e in enums)
        {
            var phpClass = phpFile.Class(e.CodeName,
                cls =>
                {
                    foreach (var v in e.Values)
                    {
                        cls.AddConstant(PhpUtil.MakeValidIdentifier(v.Name), v.Value);
                    }
                });
        }
    }
}
