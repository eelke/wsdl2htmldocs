﻿using System.Text;
using System.Xml;

namespace wsdl2htmldocs.phpgenerator;

internal static class PhpUtil
{
    /// <summary>
    ///  Maps characters that are not allowed in PHP identifiers like minus and space to underscores.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static string AsIdent(string id)
    {
        string s = id.Replace('-', '_').Replace(' ', '_');
        if (s == "print") s += '_';
        return s;
    }

    public static string AsIdent(XmlQualifiedName qname)
    {
        return AsIdent(qname.Name);
    }

    public static string MakeValidIdentifier(string name)
    {
        if (string.IsNullOrEmpty(name))
            throw new ArgumentException("name cannot be null or empty");

        // PHP documenteerd in ascii termen wat geldige identifier is.
        // We schrijven de files als UTF8 waardoor PHP de UTF8 bytes als ascii karakters ziet
        byte[] bytes = Encoding.UTF8.GetBytes(name);
        switch (bytes[0])
        {
            case (byte)'_':
            case >= (byte)'A' and <= (byte)'Z':
            case >= (byte)'a' and <= (byte)'z':
            case >= 0x80:
                return name;

            default:
                return "_" + name;
        }
    }
}
