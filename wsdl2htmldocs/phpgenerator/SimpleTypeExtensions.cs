﻿using wsdl2htmldocs.naming;
using wsdl2htmldocs.phpgenerator.config;
using wsdl2htmldocs.phpgenerator.ir;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.phpgenerator;

public static class SimpleTypeExtensions
{
    public static IrEnum ToIrEnum(this SimpleType simple, PhpGeneratorConfiguration configuration)
    {
        if (simple.Restriction is null)
            throw new ArgumentException("Can only convert simple types with a restriction to en Enum");

        return new IrEnum()
        {
            WsdlName = simple.Name.Name,
            CodeName = configuration.AsTypeIdent(simple.Name),
            Values = GenerateEnumValues(simple.Name.Name,
                simple.Restriction.Enumerations.Select(v => v.Value).ToList())
        };
    }


    private static List<IrEnum.EnumValue> GenerateEnumValues(string enumName, List<string> enumValues)
    {
        // Consider different naming styles CamelCase vs Separator based.

        // Detect if name follows convention and convert to normalized form
        var en = new NormalizedName(enumName);

        var nl = new List<NormalizedName>();
        foreach (var v in enumValues)
        {
            nl.Add(new NormalizedName(v));
        }
        bool allStartsWith = true;
        foreach (var nv in nl)
        {
            if (!nv.StartsWith(en))
            {
                allStartsWith = false;
                break;
            }
        }
        if (allStartsWith)
        {
            var l = new List<NormalizedName>();
            foreach (var nv in nl)
            {
                l.Add(nv.SubParts(en.Parts.Count));
            }
            nl = l;
        }

        var result = new List<IrEnum.EnumValue>();
        for (int i = 0; i < nl.Count; ++i)
        {
            result.Add(new IrEnum.EnumValue(nl[i].ToSnakeCase().ToUpperInvariant(), enumValues[i]));
        }
        return result;
    }
}
