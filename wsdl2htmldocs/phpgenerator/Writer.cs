﻿using System.IO;

namespace wsdl2htmldocs.phpgenerator;

public class Writer
{
    private TextWriter writer;
    //private string ident = string.Empty;
    private char[] indentTabs = { '\t', '\t', '\t', '\t', '\t', '\t', '\t', '\t' };
    private int indentSize = 0;

    public Writer(TextWriter writer)
    {
        this.writer = writer;
    }

    public void WriteLine(string line)
    {
        WriteIndent();
        writer.WriteLine(line);
    }

    public void WriteIndent()
    {
        writer.Write(indentTabs, 0, indentSize);
    }

    public void WriteRaw(string raw)
    {
        writer.Write(raw);
    }

    public IdentGuard IncreaseIdent()
    {
        ++indentSize;
        return new IdentGuard(this);
    }

    protected void DecreaseIdent()
    {
        if (indentSize > 0)
        {
            indentSize--;
        }
    }

    public class IdentGuard : IDisposable
    {
        private readonly Writer _writer;

        public IdentGuard(Writer writer)
        {
            _writer = writer;
        }

        public void Dispose()
        {
            _writer.DecreaseIdent();
        }
    }
}
