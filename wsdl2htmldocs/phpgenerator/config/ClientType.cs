﻿namespace wsdl2htmldocs.phpgenerator.config;

public class ClientType
{
    public string? Path { get; set; }
    public string? Namespace { get; set; }
    /// <summary>
    /// Name for the client class
    /// </summary>
    public string? ClassName { get; set; }
}
