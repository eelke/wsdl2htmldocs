﻿namespace wsdl2htmldocs.phpgenerator.config;

public class FrameworkType
{
    public string? Path { get; set; }
    public string? Namespace { get; set; }
}
