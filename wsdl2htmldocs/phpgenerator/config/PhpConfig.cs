﻿using wsdl2htmldocs;
using wsdl2htmldocs.sidecar;

namespace wsdl2htmldocs.phpgenerator.config;

public class ListWrapperDetection
{
    /// <summary>
    /// When this list is empty any type that is complex and has one single repeating item is considered 
    /// a list wrapper. Use a list of postfixes to make the detection less greedy.
    /// </summary>
    public List<string> AllowedTypeNamePostfixes { get; set; } = new List<string>();
}

public class PhpConfig
{
    public ClientType? Client { get; set; }
    public FrameworkType? Framework { get; set; }
    public List<sidecar.Type> Types { get; set; } = new();
    public List<NameMapping>? TypeNameMappings { get; set; }
    public ListWrapperDetection? ListWrapperDetection { get; set; }

    public sidecar.Type? FindType(string wsdlName)
    {
        if (Types == null)
            return null;

        foreach (var t in Types)
        {
            if (t.WsdlName == wsdlName)
                return t;
        }
        return null;
    }
}
