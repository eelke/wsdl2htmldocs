﻿using System.IO;
using System.IO.Abstractions;
using System.Xml;
using wsdl2htmldocs.sidecar;
using wsdl2htmldocs.soap;

namespace wsdl2htmldocs.phpgenerator.config;

public class PhpGeneratorConfiguration
{
    private readonly IFileSystem fileSystem;
    private readonly Dictionary<string, string> nameMappings = new();

    public GeneratePhpParams Params { get; init; }
    public WsdlSidecar Sidecar { get; init; }

    public PhpGeneratorConfiguration(GeneratePhpParams @params, WsdlSidecar sidecar, IFileSystem? fileSystem = null)
    {
        Params = @params;
        Sidecar = sidecar;
        UpdateNameMappings(Sidecar.PhpConfig?.TypeNameMappings);
        this.fileSystem = fileSystem ?? new FileSystem();
    }

    public IFileInfo GetOutputFile(string filename)
    {
        return fileSystem.FileInfo.New(GetOutputPath() + Path.DirectorySeparatorChar + filename);
    }

    public bool IsListWrapper(ComplexType ct)
    {
        if (ct.Elements.Count == 1 && ct.Elements[0].IsArray && ct.Name != null)
        {
            var postfixes = Sidecar.PhpConfig.ListWrapperDetection?.AllowedTypeNamePostfixes;
            if (postfixes == null || !postfixes.Any())
            {
                return true;
            }
            foreach (var postfix in postfixes)
            {
                if (ct.Name.Name.EndsWith(postfix))
                    return true;
            }
        }
        return false;
    }


    /// <summary>
    /// Retourneerd de namespace for framework code
    /// </summary>
    /// <param name="with_trailing_separator">Add a slash at the end unless empty</param>
    /// <returns></returns>
    public string GetFrameworkNamespace(bool with_trailing_separator = false)
    {
        string ns = Sidecar.PhpConfig?.Framework?.Namespace ?? "Wsdl2PhpGenerator";
        if (with_trailing_separator && ns != "")
        {
            ns += @"\";
        }
        return ns;
    }

    public string GetClientNamespace(Wsdl wsdl, bool with_trailing_separator = false)
    {
        string ns = Sidecar.PhpConfig?.Client?.Namespace ?? PhpUtil.AsIdent(wsdl.Service.Name);
        if (with_trailing_separator && ns != "")
        {
            ns += @"\";
        }
        return ns;
    }

    public string GetSoapClientClassName()
    {
        return Sidecar.PhpConfig?.Client?.ClassName ?? "SoapClient";
    }

    public string GetSoapClientFileName()
    {
        if (Params.Output != null)
        {
            return Path.GetFileName(Params.Output);
        }
        else
        {
            return GetSoapClientClassName() + ".php";
        }
    }

    public string GetFrameworkPath()
    {
        string? sc_path = Sidecar.PhpConfig?.Framework?.Path;
        if (sc_path != null)
        {
            if (Path.IsPathFullyQualified(sc_path))
            {
                return sc_path;
            }
            return GetBaseOutputPath() + Path.DirectorySeparatorChar + sc_path;
        }
        return GetBaseOutputPath() + Path.DirectorySeparatorChar + "Wsdl2PhpGenerator";
    }


    /// <summary>
    /// Returns the root for all relative output paths.
    /// </summary>
    /// <returns></returns>
    private string GetBaseOutputPath()
    {
        if (Params.OutputBasepath != null)
        {
            return Params.OutputBasepath.FullName;
        }
        if (Params.Output != null)
        {
            var p = Path.GetDirectoryName(Params.Output);
            if (p != null)
                return p;
        }
        return Directory.GetCurrentDirectory();
    }

    public string GetOutputPath()
    {
        var path = GetPathFromOutputParam();
        if (path != null)
        {
            return path;
        }

        var sc_path = Sidecar.PhpConfig.Client?.Path;
        if (sc_path != null)
        {
            if (Path.IsPathFullyQualified(sc_path))
            {
                return sc_path;
            }
            return GetBaseOutputPath() + Path.DirectorySeparatorChar + sc_path;
        }
        return GetBaseOutputPath();
    }

    public string AsTypeIdent(XmlQualifiedName qname)
    {
        if (nameMappings.TryGetValue(qname.Name, out var n))
        {
            return n;
        }
        return PhpUtil.AsIdent(qname.Name);
    }

    private string? GetPathFromOutputParam()
    {
        if (Params.Output != null)
        {
            if (Path.GetFileName(Params.Output) != Params.Output)
            {
                return Path.GetDirectoryName(Params.Output);
            }
        }
        return null;
    }

    private void UpdateNameMappings(List<NameMapping>? mappings)
    {
        nameMappings.Clear();
        if (mappings != null)
        {
            foreach (var m in mappings)
            {
                nameMappings.Add(m.WsdlName, m.CodeName);
            }
        }
    }

}
