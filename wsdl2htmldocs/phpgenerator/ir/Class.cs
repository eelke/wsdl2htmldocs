﻿namespace wsdl2htmldocs.phpgenerator.ir;

class Class
{
    public string PhpName { get; set; }
    public string? Extends { get; set; }
    public string WsdlName { get; set; }
    public List<Property> DeclareProperties { get; set; } = new List<Property>();
    public List<PropertyReader> ReadProperties { get; set; } = new List<PropertyReader>();
    public List<PropertyWriter> WriteProperties { get; set; } = new List<PropertyWriter>();
    public List<PostDeserializeAction> PostDeserializeActions { get; set; } = new List<PostDeserializeAction>();
    /// <summary>
    /// Names a file to copy into the object.
    /// </summary>
    public string? ImplementationFile { get; set; }

    public Class(string phpName, string wsdlName)
    {
        PhpName = phpName;
        WsdlName = wsdlName;
    }
}
