﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wsdl2htmldocs.phpgenerator.ir;
internal class ClassList : IEnumerable<Class>
{
    private readonly Dictionary<string, Class> byPhpName = new();

    public int Count => byPhpName.Count;

    public void Clear()
    {
        byPhpName.Clear();
    }

    public void Add(Class cls)
    {
        byPhpName.Add(cls.PhpName, cls);
    }

    public Class GetByPhpName(string phpName)
    {
        if (byPhpName.TryGetValue(phpName, out var cls))
        {
            return cls;
        }
        else
        {
            throw new ApplicationException($"Could not find class {phpName}");
        }
    }

    public IEnumerator<Class> GetEnumerator()
    {
        return byPhpName.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
