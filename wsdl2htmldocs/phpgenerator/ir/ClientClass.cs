﻿namespace wsdl2htmldocs.phpgenerator.ir;

internal class ClientOperation
{
    public required string OperationName { get; set; }
    public Class? RequestClass { get; set; }
    public required string ResultTypeName { get; set; }
}

internal class ClientClass
{
    public required string ClassName { get; set; }

    public List<ClientOperation> Operations { get; } = [];
}
