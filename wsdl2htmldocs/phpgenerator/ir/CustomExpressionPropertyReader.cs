﻿namespace wsdl2htmldocs.phpgenerator.ir;

class CustomExpressionPropertyReader : PropertyReader
{
    private string expression;

    public CustomExpressionPropertyReader(Property property, string expression) : base(property)
    {
        this.expression = expression;
    }

    public override string GetDeserializeCode()
    {
        return expression;
    }
}
