﻿namespace wsdl2htmldocs.phpgenerator.ir;

class CustomExpressionPropertyWriter : PropertyWriter
{
    private Property ObjectField;
    private string expression;

    public CustomExpressionPropertyWriter(Property objectField, string expression)
    {
        ObjectField = objectField;
        this.expression = expression;
    }

    public override string GetSerializationCode()
    {
        string code = string.Empty;
        if (ObjectField.Optional)
        {
            code += $"if ($this->{ObjectField.Name} !== null) ";
        }
        code += expression;
        return code;
    }
}
