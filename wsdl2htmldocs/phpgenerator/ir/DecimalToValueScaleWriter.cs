﻿namespace wsdl2htmldocs.phpgenerator.ir;

/// <summary>
/// This type can only be used for writing.
/// TODO better separation between reading and writing properties
/// </summary>
class DecimalToValueScalePropertyWriter : PropertyWriter
{
    public required Property ObjectField { get; set; }
    /// <summary>
    /// Field to which to write the unscaled value, should be a long field
    /// </summary>
    public required string ValueField { get; set; }
    /// <summary>
    /// Field to write the scale to
    /// </summary>
    public required string ScaleField { get; set; }
    /// <summary>
    /// If no ScaleField set this value is used for the scale
    /// </summary>
    public required int DefaultScale { get; set; }

    public override string GetSerializationCode()
    {
        string code = string.Empty;
        if (ObjectField.Optional)
        {
            code += $"if ($this->{ObjectField.Name} !== null) ";
        }

        code += $"$gen->writeValueAndScale('{ValueField}', '{ScaleField}', $this->{ObjectField.Name}, {DefaultScale});";
        return code;
    }

}
