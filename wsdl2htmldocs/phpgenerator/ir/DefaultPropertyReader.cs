﻿namespace wsdl2htmldocs.phpgenerator.ir;

class DefaultPropertyReader : PropertyReader
{
    public DefaultPropertyReader(Property property) : base(property)
    {
    }

    public override string GetDeserializeCode()
    {
        string target = Property.DeserializeLocal ? $"${Property.Name}" : $"$o->{Property.Name}";
        return Property.Type.GetDeserializeCode(target, "$in");
    }
}
