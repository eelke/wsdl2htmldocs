﻿namespace wsdl2htmldocs.phpgenerator.ir;

/// <summary>
/// PropertyWriter that uses the simple property declarations to generate the writer code.
/// </summary>
class DefaultPropertyWriter: PropertyWriter
{
    public Property Property { get; set; }

    public DefaultPropertyWriter(Property property)
    {
        Property = property;
    }

    public override string GetSerializationCode()
    {
        string code = string.Empty;
        if (Property.Optional)
        {
            code += $"if ($this->{Property.Name} !== null) ";
        }

        code += Property.Type.GetSerializeCode($"$this->{Property.Name}", Property.ElemName);
        return code;
    }
}
