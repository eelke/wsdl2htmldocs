﻿namespace wsdl2htmldocs.phpgenerator.ir;

public class IrEnum
{
    public required string CodeName { get; set; }
    public required string WsdlName { get; set; }
    public required List<EnumValue> Values { get; set; }

    public class EnumValue
    {
        /// <summary>
        /// The enum value name in the PHP code
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// The enum soap value
        /// </summary>
        public string Value { get; private set; }

        public EnumValue(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
