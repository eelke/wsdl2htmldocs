﻿namespace wsdl2htmldocs.phpgenerator.ir;

class ListWrapperPropertyReader : PropertyReader
{
    private readonly string wrapperClassName;
    private readonly string elemName;

    public ListWrapperPropertyReader(Property property, string wrapperClassName, string elemName) : base(property)
    {
        this.wrapperClassName = wrapperClassName;
        this.elemName = elemName;
    }

    public override string GetDeserializeCode()
    {
        return $"$o->{Property.Name} = ($this->load_{wrapperClassName}($in))->{elemName}";
    }
}
