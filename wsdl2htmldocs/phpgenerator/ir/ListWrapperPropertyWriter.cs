﻿namespace wsdl2htmldocs.phpgenerator.ir;

class ListWrapperPropertyWriter : PropertyWriter
{
    private Property property;
    private string wrapperClassName;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="property">The property this writer writes</param>
    /// <param name="wrapperClassName">The classname of the wrapper object that was removed in the graph.</param>
    public ListWrapperPropertyWriter(Property property, string wrapperClassName)
    {
        this.property = property;
        this.wrapperClassName = wrapperClassName;
    }

    public override string GetSerializationCode()
    {
        string code = string.Empty;
        if (property.Optional)
        {
            code += $"if ($this->{property.Name} !== null) {{\n";
        }
        code += $"$tmp_{property.Name} = new {wrapperClassName}($this->{property.Name});\n";
        code += $"$tmp_{property.Name}->write($gen, '{property.ElemName}');\n";
        if (property.Optional)
        {
            code += "}";
        }
        return code;
    }
}
