﻿namespace wsdl2htmldocs.phpgenerator.ir;

/// <summary>
/// These actions are useful for processing locally de serialized properties because
/// at this point all data is available.
/// </summary>
class PostDeserializeAction
{
    /// <summary>
    /// This is used as a format string with
    /// {0} being the name of the object being deserialized.
    /// </summary>
    /// <example>
    /// {0}->quantity = BigDecimal::ofUnscaledValue($quantity, $decimalplaces);
    /// </example>
    public required string CodeFmt { get; set; }
}
