﻿namespace wsdl2htmldocs.phpgenerator.ir;

class Property
{
    public required Type Type { get; set; }
    public required string Name { get; set; }
    /// <summary>
    /// Name of the field in the ComplexType, this is also the name used in the SOAP messages
    /// </summary>
    public required string ElemName { get; set; }
    public required bool Optional { get; set; }
    /// <summary>
    /// Signals this property should be deserialized to a local variable.
    /// Possibly because it is used in a post deserialize action.
    /// </summary>
    public bool DeserializeLocal { get; set; } = false;

    /*
    internal string GetDeclaration()
    {
        string th = Type.GetTypeHint();

        string result = "public ";
        if (!string.IsNullOrEmpty(th))
        {
            if (Optional)
            {
                result += "?";
            }
            result += th;
            result += " ";
        }

        result += "$" + Name;

        if (Optional)
        {
            result += " = null";
        }
        else
        {
            string i = Type.GetDeclarationInitializer();
            if (!string.IsNullOrEmpty(i))
                result += $" = {i}";
        }
        result += ";";
        return result;
    }
    */

    internal void WriteDeclaration(Writer writer)
    {
        string th = Type.GetTypeHint();

        writer.WriteRaw("public ");
        if (!string.IsNullOrEmpty(th))
        {
            if (Optional)
            {
                writer.WriteRaw("?");
            }
            writer.WriteRaw(th);
            writer.WriteRaw(" ");
        }

        writer.WriteRaw("$");
        writer.WriteRaw(Name);

        if (Optional)
        {
            writer.WriteRaw(" = null");
        }
        else
        {
            string i = Type.GetDeclarationInitializer();
            if (!string.IsNullOrEmpty(i))
            {
                writer.WriteRaw(" = ");
                writer.WriteRaw(i);
            }
        }
        writer.WriteRaw(";\n");
    }
}
