﻿namespace wsdl2htmldocs.phpgenerator.ir;

abstract class PropertyReader
{
    public Property Property { get; private set; }

    protected PropertyReader(Property property)
    {
        this.Property = property;
    }

    public abstract string GetDeserializeCode();
}
