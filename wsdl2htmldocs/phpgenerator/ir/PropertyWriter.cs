﻿namespace wsdl2htmldocs.phpgenerator.ir;

abstract class PropertyWriter
{
    public abstract string GetSerializationCode();
}
