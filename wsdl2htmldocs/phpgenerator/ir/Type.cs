﻿namespace wsdl2htmldocs.phpgenerator.ir;

abstract class Type
{
    //public string FieldName { get; set; }
    public abstract string GetTypeHint();
    public virtual string GetDeclarationInitializer()
    {
        return string.Empty;
    }
    public virtual string GetConstructorInitializer()
    {
        return string.Empty;
    }

    public abstract string GetDeserializeCode(string target, string source);

    public abstract string GetSerializeCode(string prop, string elemName);
}
