﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeArray : Type
{
    public Type ContainedType { get; set; }

    public TypeArray(Type containedType)
    {
        ContainedType = containedType;
    }

    public override string GetDeclarationInitializer()
    {
        return "array()";
    }

    public override string GetDeserializeCode(string target, string source)
    {
        return ContainedType.GetDeserializeCode($"{target}[]", source);
    }

    public override string GetSerializeCode(string prop, string elemname)
    {
        string elem_code = ContainedType.GetSerializeCode("$elem", elemname);
        string result = $"foreach ({prop} as $elem) {elem_code}";
        return result;
    }

    public override string GetTypeHint()
    {
        return string.Empty;
    }
}
