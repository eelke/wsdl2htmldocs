﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeBool : Type
{
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_bool_property({source})";
    }

    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->writeBool('{elemname}', {prop});";
    }
    public override string GetTypeHint()
    {
        return "bool";
    }
}
