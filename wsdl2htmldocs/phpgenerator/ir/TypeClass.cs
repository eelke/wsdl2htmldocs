﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeClass : Type
{
    /// <summary>
    /// Name of the class in the PHP code
    /// </summary>
    public required string ClassName { get; set; }
    /// <summary>
    /// Initialy this field is left null.
    /// It is set in the resolve phase before the actual php code
    /// is generated. So during code generation you can assume it has been set.
    /// </summary>
    //public Class Definition { get; set; } = null;

    public override string GetConstructorInitializer()
    {
        return $"new {ClassName}()";
    }
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_{ClassName}({source})";
    }
    public override string GetSerializeCode(string prop, string elemName)
    {
        return $"{prop}->write($gen, '{elemName}');";
    }
    public override string GetTypeHint()
    {
        return ClassName;
    }
}
