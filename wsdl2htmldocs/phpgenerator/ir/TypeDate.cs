﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeDate : Type
{
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_Date_property({source})";
    }

    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->writeDate('{elemname}', {prop});";
    }
    public override string GetTypeHint()
    {
        return @"\DateTime";
    }
}
