﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeDateTime : Type
{
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_DateTime_property({source})";
    }
    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->writeDateTime('{elemname}', {prop});";
    }
    public override string GetTypeHint()
    {
        return @"\DateTime";
    }
}
