﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeDecimal : Type
{
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_BigDecimal_property({source})";
    }
    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->writeBigDecimal('{elemname}', {prop});";
    }
    public override string GetTypeHint()
    {
        return "BigDecimal";
    }
}
