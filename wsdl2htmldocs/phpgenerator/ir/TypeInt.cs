﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeInt : Type
{

    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_int_property({source})";
    }
    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->writeInt('{elemname}', {prop});";
    }
    public override string GetTypeHint()
    {
        return "int";
    }
}
