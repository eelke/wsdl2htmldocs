﻿namespace wsdl2htmldocs.phpgenerator.ir;

class TypeString : Type
{
    public override string GetDeserializeCode(string target, string source)
    {
        return $"{target} = $this->load_string_property({source})";
    }
    public override string GetSerializeCode(string prop, string elemname)
    {
        return $"$gen->out->writeElementNs(self::TNS, '{elemname}', null, {prop});";
    }
    public override string GetTypeHint()
    {
        return "string";
    }
}
