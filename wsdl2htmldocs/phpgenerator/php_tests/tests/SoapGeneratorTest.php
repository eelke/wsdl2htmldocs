<?php declare(strict_types=1);
require_once 'gen/soapclient.php';
use PHPUnit\Framework\TestCase;

final class SoapGeneratorTest extends TestCase {

    private $g;

    protected function setUp() : void {
        $this->g = new Test_API\SoapGenerator(true);
    }

    private function parseXml(string $s) : SimpleXMLElement {
        $xml = new SimpleXMLElement($s);
        $xml->registerXPathNamespace('s', 'http://schemas.xmlsoap.org/soap/envelope/');
        $xml->registerXPathNamespace('n', 'urn:mplusqapi');
        return $xml;
    }

    public function testGen1() : void {
        $address = new Test_API\Address();
        $address->name = 'Foo Bar';
        
        $res = $this->g->write($address, 'address');

        $xml = $this->parseXml($res);
        $result = $xml->xpath('/s:Envelope/s:Body/n:address/n:name/text()')[0];
        $this->assertEquals($result, 'Foo Bar');
    }

    public function testDate() : void {
        $r = new Test_API\DateTestType();
        $r->test = DateTime::createFromFormat('Y-m-d|', '2019-8-1');

        $res = $this->g->write($r, 'request');
        $xml = $this->parseXml($res);
        $result = $xml->xpath('/s:Envelope/s:Body/n:request/n:test/text()')[0];
        $this->assertEquals('2019-08-01', $result);
    }
    public function testDateTime() : void {
        $r = new Test_API\DateTimeTestType();
        $r->test = DateTime::createFromFormat('Y-m-d H:i:s', '2019-8-1 8:23:34');

        $res = $this->g->write($r, 'request');
        $xml = $this->parseXml($res);
        $result = $xml->xpath('/s:Envelope/s:Body/n:request/n:test/text()')[0];
        $this->assertEquals('2019-08-01T08:23:34', $result);
    }
}
