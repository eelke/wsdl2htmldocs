<?php declare(strict_types=1);
require_once 'gen/soapclient.php';
use PHPUnit\Framework\TestCase;

final class SoapParserTest extends TestCase {
    private $input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns="urn:mplusqapi">
	<SOAP-ENV:Header/>
	<SOAP-ENV:Body>
		<ns:GetArticleVariantsResponse>
			<ns:result>GET-ARTICLE-VARIANTS-RESULT-OK</ns:result>
			<ns:articleVariants>
				<ns:variant>
					<ns:articleVariantId>22</ns:articleVariantId>
					<ns:description>Appel M</ns:description>
					<ns:receiptText/>
					<ns:translatedReceiptText/>
					<ns:invoiceText/>
					<ns:barcode/>
					<ns:barcodeDate>2020-12-08</ns:barcodeDate>
					<ns:quantity>1.000</ns:quantity>
					<ns:priceIncl>3.50</ns:priceIncl>
					<ns:isCurrent>false</ns:isCurrent>
					<ns:quantityInPackaging>1.000</ns:quantityInPackaging>
					<ns:packagingType/>
					<ns:orderQuantity>11.123</ns:orderQuantity>
					<ns:articleNumberSupplier/>
					<ns:packagingContent/>
					<ns:packaging/>
				</ns:variant>
			</ns:articleVariants>
		</ns:GetArticleVariantsResponse>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOT;

	private $parseResult;

	protected function setUp(): void {
		$soapParser = new Test_API\SoapParser();
		$this->parseResult = $soapParser->parse($this->input);
	}

    public function testCorrectResponseType() : void {
        $this->assertInstanceOf(Test_API\GetArticleVariantsResponse::class, $this->parseResult);
	}
	
	public function testHasArray() : void {
		$this->assertIsArray($this->parseResult->articleVariants->variant);
	}

	public function testBigDecimal() : void {
		$v = $this->parseResult->articleVariants->variant[0];
		$this->assertEquals($v->orderQuantity->getIntegralPart(), '11');
		$this->assertEquals($v->orderQuantity->getFractionalPart(), '123');
	}

	public function testDate() : void {
		$v = $this->parseResult->articleVariants->variant[0];
		$expected = new DateTime();
		$expected->setDate(2020, 12, 8);
		$expected->setTime(0, 0);
		$this->assertEquals($expected, $v->barcodeDate);
	}

	public function testDateTime() : void {
		$input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns="urn:mplusqapi">
	<SOAP-ENV:Header/>
	<SOAP-ENV:Body>
		<ns:DateTimeTestType>
			<ns:test>2020-04-02T09:05:16</ns:test>
		</ns:DateTimeTestType>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOT;
		$soapParser = new Test_API\SoapParser();
		$parseResult = $soapParser->parse($input);
		$v = $parseResult->test;
		$expected = new DateTime();
		$expected->setDate(2020, 4, 2);
		$expected->setTime(9, 5, 16);
		$this->assertEquals($expected, $v);
	}



}