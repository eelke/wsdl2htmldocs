﻿using System.Xml.Linq;

namespace wsdl2htmldocs.phpgenerator.phpsourcegenerator;

public class ClassBuilder : IClassBuilder
{
    private readonly IFileBuilder fileBuilder;

    internal ClassBuilder(IFileBuilder fileBuilder)
    {
        this.fileBuilder = fileBuilder;
    }

    public IClassBuilder AddConstant(string name, string value)
    {
        fileBuilder.Writer.WriteLine($"const {name} = '{value}';");
        return this;
    }
}

