﻿using System.IO;

namespace wsdl2htmldocs.phpgenerator.phpsourcegenerator;


public class FileBuilder : IFileBuilder
{
    public Writer Writer { get; init; }
    //private readonly Writer? writer = null;

    public IBuilderFactory BuilderFactory { get; init; }

    public FileBuilder(TextWriter writer, string? namespaceName = null, IBuilderFactory? builderFactory = null)
    {
        Writer = new Writer(writer);
        BuilderFactory = builderFactory ?? new DefaultBuilderFactory();
        writer.WriteLine("""
            <?php declare(strict_types=1);
            // Generated code
            """);
        if (!string.IsNullOrEmpty(namespaceName))
        {
            Writer.WriteLine($"namespace {namespaceName};");
        }
    }

    public IFileBuilder Class(string className, Action<IClassBuilder> classContentActions)
    {
        return Class(className, null, classContentActions);
    }

    public IFileBuilder Class(string className, string? baseClass, Action<IClassBuilder> classContentActions)
    {
        Writer.WriteLine($"class {className} {{");
        using (var guard = Writer.IncreaseIdent())
        {
            classContentActions(BuilderFactory.ClassBuilder(this));
        }
        Writer.WriteLine("}");
        return this;
    }
}

