﻿namespace wsdl2htmldocs.phpgenerator.phpsourcegenerator;

public interface IBuilderFactory
{
    IClassBuilder ClassBuilder(IFileBuilder fileBuilder);
}