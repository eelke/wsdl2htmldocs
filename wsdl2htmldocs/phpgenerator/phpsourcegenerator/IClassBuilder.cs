﻿namespace wsdl2htmldocs.phpgenerator.phpsourcegenerator;

public interface IClassBuilder
{
    IClassBuilder AddConstant(string name, string value);
}