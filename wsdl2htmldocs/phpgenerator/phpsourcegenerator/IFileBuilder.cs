﻿using System.IO;

namespace wsdl2htmldocs.phpgenerator.phpsourcegenerator;

public interface IFileBuilder
{
    Writer Writer { get; }
    IBuilderFactory BuilderFactory { get; }

    IFileBuilder Class(string className, Action<IClassBuilder> classContentActions);
    IFileBuilder Class(string className, string? baseClass, Action<IClassBuilder> classContentActions);
}