﻿namespace wsdl2htmldocs.sidecar;

/// <summary>
/// Allows for mapping a complexType from the WSDL to a pre existing type.
/// The type from the wsdl is still generated to implement the (de)serialization functions
/// But all fiels of that type will be the pre existing type.
/// </summary>
public class ConversionConfig
{
    /// <summary>
    /// Typename to declare properties with instead of the WSDL derived type
    /// </summary>
    public string TargetType { get; set; } = "";

    /// <summary>
    /// Expression that return TargetType.
    /// Should contain a {0} place holder for receiving the input value
    /// This code will execute
    /// </summary>
    public string ConvertToExpressionFmt { get; set; } = "";
    /// <summary>
    /// Expression that return Original type.
    /// Should contain a {0} place holder for receiving the TargetType value that needs to be converted back.
    /// </summary>
    public string ConvertFromExpressionFmt { get; set; } = "";

}
