﻿using System.Xml.Serialization;

namespace wsdl2htmldocs.sidecar;

[Serializable]
public enum MappingType
{
    /// <summary>
    /// Direct mappings is the standard. It can be specified in the sidecar to prevent automatic mappings.
    /// </summary>
    [XmlEnum]
    Direct,
    [XmlEnum]
    LongToDecimal
}

public class Mapping
{
    [XmlAttribute]
    public MappingType Type { get; set; }
    public string? Field { get; set; }
    public string? Scale { get; set; }
    public int DefaultDecimals { get; set; }

    public static Mapping DirectMappingInstance = new Mapping() { Type = MappingType.Direct };
}
