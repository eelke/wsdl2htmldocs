﻿namespace wsdl2htmldocs.sidecar;

public class NameMapping
{
    public string WsdlName { get; set; } = "";
    public string CodeName { get; set; } = "";
}
