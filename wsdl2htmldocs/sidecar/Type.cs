﻿using System.Xml.Serialization;

namespace wsdl2htmldocs.sidecar;

public class Type
{
    [XmlAttribute]
    public string WsdlName { get; set; } = "";

    public ConversionConfig? Conversion { get; set; }
    public List<Mapping> Mappings { get; set; } = [];
    /// <summary>
    /// A file that contains extra declarations to be included in the object.
    /// </summary>
    public string ImplementationFile { get; set; } = "";

    public Mapping? FindMappingForWsdlField(string field)
    {
        foreach (var m in Mappings)
        {
            if (m.Field == field || m.Scale == field)
                return m;
        }
        return null;
    }
}
