﻿using System.IO;
using System.Xml.Serialization;
using wsdl2htmldocs.phpgenerator.config;

namespace wsdl2htmldocs.sidecar;

public class WsdlSidecar
{
    public PhpConfig PhpConfig { get; set; } = new();

    public static WsdlSidecar? Load(string filename)
    {
        XmlSerializer deserializer = new XmlSerializer(typeof(WsdlSidecar));
        TextReader textReader = new StreamReader(filename);
        WsdlSidecar? sidecar = (WsdlSidecar?)deserializer.Deserialize(textReader);
        textReader.Close();
        return sidecar;
    }
}
