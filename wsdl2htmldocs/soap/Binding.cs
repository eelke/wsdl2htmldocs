﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Binding
{
    public string Name { get; set; } = string.Empty;
    public XmlQualifiedName? Type { get; set; }

    internal void Merge(Binding binding)
    {
        if (string.IsNullOrEmpty(Name))
            Name = binding.Name;

        if (Type == null)
            Type = binding.Type;
    }

    // Note we are not reading any more details because we assume, if we are going to read
    // more details we should probably add them to the existing list of operations.
    //<SOAP:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
    //<operation name="?">
    //  <SOAP:operation soapAction=""/>
    //  <input>
    //        <SOAP:body use="literal" parts="Body"/>
    //  </input>
    //  <output>
    //        <SOAP:body use="literal" parts="Body"/>
    //  </output>
    //</operation>
}