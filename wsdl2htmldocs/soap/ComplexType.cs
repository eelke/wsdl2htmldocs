﻿#nullable enable
using System.Xml;

namespace wsdl2htmldocs.soap;

/// <summary>
/// For now we only support complexTypes containing either a sequence or complexContent/extension.
/// 
/// Instead of duplicating the whole complexContent/extension structure from XMLSchema we simplify it here
/// by putting the sequence of elements from the extension in our normal Elements list en registering the base
/// in BaseTypeName.
/// </summary>
public class ComplexType : Type
{
    public ComplexType(XmlQualifiedName? name)
        : base(name)
    {
    }
    public XmlQualifiedName? BaseTypeName { get; set; }
    public List<Element> Elements { get; } = new List<Element>();

    public Element? Owner { get; set; } // set in case of anonymous type inside element


    public override void accept(ITypeVisitor visitor)
    {
        visitor.visitComplexType(this);
    }

    public ComplexType? GetBaseType(Wsdl wsdl)
    {
        if (BaseTypeName == null)
            return null;

        // AFAIK it should allways be a complextype.
        var t = wsdl.Types.Find(BaseTypeName) as ComplexType;
        if (t != null)
        {
            return t;
        }
        throw new ApplicationException("Type not found or not a ComplexType");
    }
}
