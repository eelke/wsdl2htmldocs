﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Element
{
    // Elements have a Name and Type or a Ref 
    public XmlQualifiedName Name { get; }
    public XmlQualifiedName? Ref { get; set; }
    public XmlQualifiedName? Type { get; set; }
    public Type? AnonymousType { get; set; } // Filled when instead of refering to a type it directly contains a type definition
    public int MinOccurs { get; set; } = 1;
    public int MaxOccurs { get; set; } = 1;
    public string Documentation { get; set; } = "";

    public bool IsArray => MaxOccurs != 1;
    public bool IsOptional => MinOccurs == 0 && MaxOccurs == 1;

    public Element(XmlQualifiedName name)
    {
        Name = name;
    }
}
