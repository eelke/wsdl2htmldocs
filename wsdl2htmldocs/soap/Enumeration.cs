﻿namespace wsdl2htmldocs.soap;

public class Enumeration
{
    public string Value { get; set; } = "";
    public string Documentation { get; set; } = "";
}
