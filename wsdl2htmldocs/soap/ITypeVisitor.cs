﻿namespace wsdl2htmldocs.soap;

public interface ITypeVisitor
{
    void visitComplexType(ComplexType ct);
    void visitSimpleType(SimpleType st);
}
