﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Message
{
    public XmlQualifiedName Name { get; }
    public List<Part> Parts { get; } = new List<Part>();

    public Message(XmlQualifiedName name)
    {
        Name = name;
    }
}
