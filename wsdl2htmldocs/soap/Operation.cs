﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Operation
{
    public XmlQualifiedName Name { get; init; }
    public XmlQualifiedName? Input { get; set; } // refers to a message
    public XmlQualifiedName? Output { get; set; } // refers to a message
    public string Documentation { get; set; } = "";

    public Operation(XmlQualifiedName name)
    {
        Name = name;
    }
}
