﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Part
{
    public XmlQualifiedName? Name { get; set; }
    public XmlQualifiedName? Element { get; set; }
    public XmlQualifiedName? Type { get; set; }

}
