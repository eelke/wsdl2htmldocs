﻿namespace wsdl2htmldocs.soap;

public class PortType
{
    public string Name { get; set; } = string.Empty;
    public List<Operation> Operations { get; } = new();

    internal void Merge(PortType portType)
    {
        if (string.IsNullOrEmpty(Name))
            Name = portType.Name;

        Operations.AddRange(portType.Operations);
    }
}