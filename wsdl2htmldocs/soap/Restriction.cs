﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class Restriction
{
    public required XmlQualifiedName Base { get; set; }
    public List<Enumeration> Enumerations { get; init; } = [];
}
