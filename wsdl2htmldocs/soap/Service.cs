﻿#nullable enable
namespace wsdl2htmldocs.soap;

public class Service
{
    public string Name { get; set; } = string.Empty;
    public string Documentation { get; set; } = string.Empty;

    public ServicePort Port { get; } = new();

    internal void Merge(Service service)
    {
        if (string.IsNullOrEmpty(Name))
            Name = service.Name;

        if (string.IsNullOrEmpty(Documentation))
            Documentation = service.Documentation;

        Port.Merge(service.Port);
    }
}
