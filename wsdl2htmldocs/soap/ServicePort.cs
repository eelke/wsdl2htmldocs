﻿#nullable enable
using System.Xml;

namespace wsdl2htmldocs.soap;

public class ServicePort
{
    public string Name { get; set; } = string.Empty;
    public XmlQualifiedName? Binding { get; set; }
    public string AddressLocation { get; set; } = string.Empty;

    internal void Merge(ServicePort port)
    {
        if (!string.IsNullOrEmpty(Name))
            return;

        Name = port.Name;
        Binding = port.Binding;
        AddressLocation = port.AddressLocation;
    }
}