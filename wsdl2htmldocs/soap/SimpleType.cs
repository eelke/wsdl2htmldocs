﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class SimpleType : Type
{
    public new XmlQualifiedName Name => base.Name!;

    public Restriction? Restriction { get; set; }

    public SimpleType(XmlQualifiedName name)
        : base(name)
    {
    }

    public override void accept(ITypeVisitor visitor)
    {
        visitor.visitSimpleType(this);
    }
}
