﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public abstract class Type
{
    public XmlQualifiedName? Name { get; }

    public string? Documentation { get; set; }

    public Type(XmlQualifiedName? name)
    {
        Name = name;
    }

    public abstract void accept(ITypeVisitor visitor);
}
