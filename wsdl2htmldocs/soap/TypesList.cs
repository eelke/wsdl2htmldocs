﻿using System.Xml;

namespace wsdl2htmldocs.soap;

public class TypesList
{
    private readonly Dictionary<string, Dictionary<string, Type>> _types = new();

    public void Add(Type type)
    {
        if (type.Name == null)
            throw new ApplicationException("type must have Name");

        if (!_types.TryGetValue(type.Name.Namespace, out Dictionary<string, Type>? ns))
        {
            ns = new Dictionary<string, Type>();
            _types.Add(type.Name.Namespace, ns);
        }

        ns.Add(type.Name.Name, type);
    }

    public Type? Find(string ns, string name)
    {
        if (_types.TryGetValue(ns, out Dictionary<string, Type>? o))
        {
            if (o.TryGetValue(name, out Type? t))
            {
                return t;
            }
        }
        return null;
    }

    public Type? Find(XmlQualifiedName? name)
    {
        if (name == null)
            return null;
        return Find(name.Namespace, name.Name);
    }

    public IEnumerable<string> Namespaces()
    {
        return _types.Keys.AsEnumerable();
    }

    public IEnumerable<string> TypeNames(string ns)
    {
        if (_types.TryGetValue(ns, out Dictionary<string, Type>? o))
        {
            return o.Keys.AsEnumerable();
        }
        return Enumerable.Empty<string>();
    }

    public IEnumerable<Type> Types(string ns)
    {
        if (_types.TryGetValue(ns, out Dictionary<string, Type>? o))
        {
            return o.Values.AsEnumerable();
        }
        return Enumerable.Empty<Type>();
    }

    public void Merge(TypesList source)
    {
        foreach (var ns in source._types)
        {
            if (_types.TryGetValue(ns.Key, out var typesDest))
            {
                foreach (var type in ns.Value)
                    if (!typesDest.TryAdd(type.Key, type.Value))
                        Console.WriteLine($"Duplicate type {ns.Key}:{type.Key}");
            }
            else
                _types.Add(ns.Key, ns.Value);
        }
    }
}
