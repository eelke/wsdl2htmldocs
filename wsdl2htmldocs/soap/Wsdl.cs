﻿using System.Xml;
#nullable enable

namespace wsdl2htmldocs.soap;

public class Wsdl
{
    public string Name { get; set; } = "";
    public string TargetNamespace { get; set; } = "";
    public string? SchemaTargetNamespace { get; set; }
    public Dictionary<string, string> NamespaceAliasses { get; } = new();

    public Service Service { get; } = new();

    public Dictionary<XmlQualifiedName, Message> Messages { get; } = new();

    public PortType PortType { get; } = new();
    public TypesList Types { get; } = new();
    public List<Element> Elements { get; } = new();

    public Binding Binding { get; } = new();

    public Element followReferences(Element elem)
    {
        while (elem.Ref != null)
        {
            var e = Elements.Find(e => e.Name == elem.Ref);
            if (e == null)
                throw new ApplicationException($"Could not find element for ref {elem.Ref}");
            elem = e;
        }
        return elem;
    }

    public void Merge(Wsdl input)
    {
        if (string.IsNullOrEmpty(Name))
            Name = input.Name;

        if (string.IsNullOrEmpty(TargetNamespace))
            TargetNamespace = input.TargetNamespace;

        if (string.IsNullOrEmpty(SchemaTargetNamespace))
            SchemaTargetNamespace = input.SchemaTargetNamespace;

        foreach (var ns in input.NamespaceAliasses)
            NamespaceAliasses.TryAdd(ns.Key, ns.Value);

        Service.Merge(input.Service);

        foreach (var message in input.Messages)
            Messages.TryAdd(message.Key, message.Value);

        PortType.Merge(input.PortType);
        Binding.Merge(input.Binding);
        Types.Merge(input.Types);

        Elements.AddRange(input.Elements);
    }

    public string GetNamespacePrefix(string @namespace)
    {
        foreach (var ns in NamespaceAliasses)
            if (ns.Value == @namespace)
                return ns.Key;
        throw new ApplicationException($"Unknown namespace {@namespace}");
    }

    public string PrefixName(XmlQualifiedName name)
    {
        var prefix = GetNamespacePrefix(name.Namespace);
        return $"{prefix}:{name.Name}";
    }

}
